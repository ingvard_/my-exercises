package com.ingvard.controller;


import com.ingvard.engine.Func2X;
import com.ingvard.engine.Func2XParser;
import com.ingvard.engine.Levenberg;
import com.ingvard.engine.impl.Levenberg2XImpl;
import com.ingvard.model.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: maksim
 * Date: 03.11.14
 * Time: 17:46
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class ComputingController {

    @RequestMapping(value = "/solve", method = RequestMethod.POST, headers = {"Content-type=application/json"})
    public
    @ResponseBody
    Solution getSolutionInJSON(@RequestBody Functional functional) {

        /*
        func: inputFunc,
            stopParam: inputParam,
            startDots: dots,
            epsilon: activeEpsilon,
            h: levenH
         */

        //"8*x1^2+4*x1*x2+5*x2^2"
        Func2XParser func2XParser = new Func2XParser(functional.getFunc());
        Func2X func2X = func2XParser.parser();

        Solution solution = new Solution();
        Result result = new Result();
        solution.setInput(new Input(
                func2XParser.laTexFunct(func2X),
                functional.getStopParam(),
                functional.getStartDots(),
                functional.getH(),
                functional.isEpsilon()
        ));


        Levenberg levenberg = new Levenberg2XImpl(func2X, functional.getH(), result);
        levenberg.setDots(functional.getStartDots()[0], functional.getStartDots()[1]);


        if (functional.isEpsilon()) {
            double lastFunctValue = 0;
            double currentEps = Double.MAX_VALUE;
            for (int i = 0;
                 i < 100 && functional.getStopParam() < currentEps;
                 i++) {
                lastFunctValue =
                        levenberg.compDot(func2X, levenberg.getDots()[0], levenberg.getDots()[1]);
                levenberg.nextDots();
                result.getSteps().add(new Funct(levenberg.compDot(func2X, levenberg.getDots()[0], levenberg.getDots()[1]),levenberg.getDots()));
                currentEps =  Math.abs(levenberg.compDot(func2X, levenberg.getDots()[0], levenberg.getDots()[1]) - lastFunctValue);
            }

        } else {
            int iteration = (int) functional.getStopParam();
            for (int i = 0; i < iteration; i++) {
                levenberg.nextDots();
                result.getSteps().add(new Funct(levenberg.compDot(func2X, levenberg.getDots()[0], levenberg.getDots()[1]),levenberg.getDots()));
            }
        }

        solution.setResult(result);


        return solution;

    }

}