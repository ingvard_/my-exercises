package com.ingvard.model;

/**
 * Created with IntelliJ IDEA.
 * User: maksim
 * Date: 05.11.14
 * Time: 1:34
 * To change this template use File | Settings | File Templates.
 */
public class JsonResponse {
    private String status = "";
    private String errorMessage = "";

    public JsonResponse(String status, String errorMessage) {
        this.status = status;
        this.errorMessage = errorMessage;
    }
}
