package com.ingvard.model;

/**
 * Created with IntelliJ IDEA.
 * User: maksim
 * Date: 04.11.14
 * Time: 21:04
 * To change this template use File | Settings | File Templates.
 */
public class Input {
    String laTexFunc;
    double stopParam;
    double startDots[];
    double h;
    boolean epsilon;

    public Input() {
    }

    public Input(String laTexFunc, double stopParam, double[] startDots, double h, boolean epsilon) {
        this.laTexFunc = laTexFunc;
        this.stopParam = stopParam;
        this.startDots = startDots;
        this.h = h;
        this.epsilon = epsilon;
    }


    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }


    public String getLaTexFunc() {
        return laTexFunc;
    }

    public void setLaTexFunc(String laTexFunc) {
        this.laTexFunc = laTexFunc;
    }

    public double getStopParam() {
        return stopParam;
    }

    public void setStopParam(double stopParam) {
        this.stopParam = stopParam;
    }

    public double[] getStartDots() {
        return startDots;
    }

    public void setStartDots(double[] startDots) {
        this.startDots = startDots;
    }

    public boolean isEpsilon() {
        return epsilon;
    }

    public void setEpsilon(boolean epsilon) {
        this.epsilon = epsilon;
    }
}
