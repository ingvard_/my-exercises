package com.ingvard.model;

/**
 * Created with IntelliJ IDEA.
 * User: maksim
 * Date: 04.11.14
 * Time: 23:22
 * To change this template use File | Settings | File Templates.
 */

public class Functional {
    private String func;
    private double stopParam;
    private double startDots[];
    private double h;
    private boolean epsilon;

    public Functional() {
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    public String getFunc() {
        return func;
    }

    public void setFunc(String func) {
        this.func = func;
    }

    public double getStopParam() {
        return stopParam;
    }

    public void setStopParam(double stopParam) {
        this.stopParam = stopParam;
    }

    public double[] getStartDots() {
        return startDots;
    }

    public void setStartDots(double[] startDots) {
        this.startDots = startDots;
    }

    public boolean isEpsilon() {
        return epsilon;
    }

    public void setEpsilon(boolean epsilon) {
        this.epsilon = epsilon;
    }
}
