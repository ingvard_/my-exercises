package com.ingvard.model;

/**
 * Created with IntelliJ IDEA.
 * User: maksim
 * Date: 04.11.14
 * Time: 21:11
 * To change this template use File | Settings | File Templates.
 */

public class Funct {
    double fv;
    double dots[];

    public Funct(double fv, double[] dots) {
        this.fv = fv;
        this.dots = dots;
    }

    public Funct() {
    }

    public double getFv() {
        return fv;
    }

    public void setFv(double fv) {
        this.fv = fv;
    }

    public double[] getDots() {
        return dots;
    }

    public void setDots(double[] dots) {
        this.dots = dots;
    }
}
