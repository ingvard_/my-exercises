package com.ingvard.model;

/**
 * Created with IntelliJ IDEA.
 * User: maksim
 * Date: 04.11.14
 * Time: 20:31
 * To change this template use File | Settings | File Templates.
 */


public class Solution {
    Input input = new Input();
    Result result = new Result();

    public Input getInput() {
        return input;
    }

    public void setInput(Input input) {
        this.input = input;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Solution(){

    }
    public Solution(Input input, Result result) {
        this.input = input;
        this.result = result;
    }
}
