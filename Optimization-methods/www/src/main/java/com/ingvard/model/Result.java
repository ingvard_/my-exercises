package com.ingvard.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: maksim
 * Date: 04.11.14
 * Time: 21:11
 * To change this template use File | Settings | File Templates.
 */
public class Result {

    List<Funct> steps;
    String grad[];
    String hesse[];

    public Result() {
        this.steps = new ArrayList<Funct>();
    }

    public List<Funct> getSteps() {
        return steps;
    }

    public void setSteps(List<Funct> steps) {
        this.steps = steps;
    }

    public String[] getGrad() {
        return grad;
    }

    public void setGrad(String[] grad) {
        this.grad = grad;
    }

    public String[] getHesse() {
        return hesse;
    }

    public void setHesse(String[] hesse) {
        this.hesse = hesse;
    }
}
