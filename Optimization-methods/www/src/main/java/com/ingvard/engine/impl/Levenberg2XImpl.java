package com.ingvard.engine.impl;

import com.ingvard.engine.*;
import com.ingvard.model.Result;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: maksim
 * Date: 03.11.14
 * Time: 17:38
 * To change this template use File | Settings | File Templates.
 */
public class Levenberg2XImpl implements Levenberg {
    private double x1;
    private double x2;
    private double h1;
    private Func2X func2X;
    private Result result;

    @Override
    public double compDot(Func2X funct, double x, double y) {
        return
                x * x * y * y * funct.p2x12x2 +
                        x * x * y * funct.p2x1x2 +
                        x * y * y * funct.px12x2 +
                        x * y * funct.px1x2 +
                        x * x * funct.p2x1 +
                        y * y * funct.p2x2 +
                        x * funct.px1 +
                        y * funct.px2 +
                        funct.p;
    }

    public Levenberg2XImpl(Func2X func2X, double h1, Result result) {
        this.func2X = func2X;
        this.x1 = 0.0;
        this.x2 = 0.0;
        this.h1 = h1;
        this.result = result;

    }


    @Override
    public void setDots(Double... arg) {
        this.x1 = arg[0];
        this.x2 = arg[0];
    }

    @Override
    public void nextDots() {
        Func2X gradFunc2X1 = new Func2X();
        Func2X gradFunc2X2 = new Func2X();
        //dJ/dx1
        gradFunc2X1.px12x2 = 2.0 * func2X.p2x12x2;
        gradFunc2X1.px1x2 = 2.0 * func2X.p2x1x2;
        gradFunc2X1.px1 = 2.0 * func2X.p2x1;
        gradFunc2X1.px2 = func2X.px1x2;
        gradFunc2X1.p2x2 = func2X.px12x2;
        gradFunc2X1.p = func2X.px1;

        //dJ/dx2
        gradFunc2X2.p2x1x2 = 2.0 * func2X.p2x12x2;
        gradFunc2X2.p2x1 = func2X.p2x1x2;
        gradFunc2X2.px2 = 2.0 * func2X.p2x2;
        gradFunc2X2.px1 = func2X.px1x2;
        gradFunc2X2.px1x2 = 2.0 * func2X.px12x2;
        gradFunc2X2.p = func2X.px2;

        //TODO много созданий объектов
        //System.out.println(compDot(gradFunc2X1, this.x1, this.x2) + " " + compDot(gradFunc2X2, this.x1, this.x2));
        Func2XParser func2XParser = new Func2XParser();

        result.setGrad(new String[]{
                func2XParser.laTexFunct(gradFunc2X1),
                func2XParser.laTexFunct(gradFunc2X2)
        });

        Double[][] H = Hesse(gradFunc2X1, gradFunc2X2);

        //System.out.println(Arrays.deepToString(H));


        Double[][] L = {{this.h1 + H[0][0], H[0][1]}, {H[1][0], H[1][1] + this.h1}};

        Double detL = L[0][0] * L[1][1] - L[0][1] * L[1][0];

        Double[][] invL = {{L[1][1] / detL, -L[0][1] / detL}, {-L[1][0] / detL, L[0][0] / detL}};

        // System.out.println(Arrays.deepToString(invL));

        double gradX1 = compDot(gradFunc2X1, this.x1, this.x2);
        double gradX2 = compDot(gradFunc2X2, this.x1, this.x2);

        x1 = x1 - (invL[0][0] * gradX1 + invL[0][1] * gradX2);
        x2 = x2 - (invL[1][0] * gradX1 + invL[1][1] * gradX2);

        System.err.println(x1 + " " + x2);


    }

    @Override
    public double[] getDots() {
        double[] result = {x1, x2};
        return result;
    }

    Double[][] Hesse(Func2X gradFunc2X1, Func2X gradFunc2X2) {

        Double[][] H = new Double[2][2];
        Func2XParser func2XParser = new Func2XParser();

        Func2X h00 = new Func2X();
        Func2X h01 = new Func2X();
        Func2X h10 = new Func2X();
        Func2X h11 = new Func2X();

        h00.p = gradFunc2X1.px1;
        h00.px2 = gradFunc2X1.px1x2;
        h00.p2x2 = gradFunc2X1.px12x2;

        h01.px1x2 = gradFunc2X1.px12x2 * 2.0;
        h01.px1 = gradFunc2X1.px1x2;
        h01.p = gradFunc2X1.px2;
        h01.px2 = gradFunc2X1.p2x2 * 2.0;

        h10.px1x2 = gradFunc2X2.p2x1x2 * 2.0;
        h10.px1 = gradFunc2X2.p2x1 * 2.0;
        h10.p = gradFunc2X2.px1;
        h10.px2 = gradFunc2X2.px1x2;

        h11.p2x1 = gradFunc2X2.p2x1x2;
        h11.px1 = gradFunc2X2.px1x2;
        h11.p = gradFunc2X2.px2;


        result.setHesse(new String[]{
                func2XParser.laTexFunct(h00),
                func2XParser.laTexFunct(h01),
                func2XParser.laTexFunct(h10),
                func2XParser.laTexFunct(h11)
        });

        H[0][0] = this.x2 * this.x2 * gradFunc2X1.px12x2 + this.x2 * gradFunc2X1.px1x2 + gradFunc2X1.px1;

        H[0][1] = 2.0 * this.x1 * this.x2 * gradFunc2X1.px12x2
                + this.x1 * gradFunc2X1.px1x2
                + gradFunc2X1.px2
                + 2.0 * this.x2 * gradFunc2X1.p2x2;

        H[1][0] = 2.0 * this.x1 * this.x2 * gradFunc2X2.p2x1x2
                + 2.0 * this.x1 * gradFunc2X2.p2x1
                + gradFunc2X2.px1
                + this.x2 * gradFunc2X2.px1x2;

        H[1][1] = this.x1 * this.x1 * gradFunc2X2.p2x1x2 + this.x1 * gradFunc2X2.px1x2 + gradFunc2X2.px2;
        return H;
    }


}
