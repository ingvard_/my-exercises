package com.ingvard.engine;

/**
 * Created with IntelliJ IDEA.
 * User: maksim
 * Date: 03.11.14
 * Time: 17:09
 * To change this template use File | Settings | File Templates.
 */
public interface Levenberg {
    public void setDots(Double... arg);

    public void nextDots();

    public double[] getDots();

    public double compDot(Func2X funct, double x, double y);
}
