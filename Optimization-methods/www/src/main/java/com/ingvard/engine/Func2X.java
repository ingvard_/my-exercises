package com.ingvard.engine;

/**
 * Created with IntelliJ IDEA.
 * User: maksim
 * Date: 03.11.14
 * Time: 17:41
 * To change this template use File | Settings | File Templates.
 */
public class Func2X {

    public double p2x12x2;
    public double p2x1x2;
    public double px12x2;
    public double px1x2;
    public double p2x1;
    public double p2x2;
    public double px1;
    public double px2;
    public double p;

    public Func2X() {
        p2x12x2 = 0;
        p2x1x2 = 0;
        px12x2 = 0;
        px1x2 = 0;
        p2x1 = 0;
        p2x2 = 0;
        px1 = 0;
        px2 = 0;
        p = 0;
    }

}
