package com.ingvard.engine;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: maksim
 * Date: 05.11.14
 * Time: 2:06
 * To change this template use File | Settings | File Templates.
 */
public class Func2XParser {
    private String funct;

    public Func2XParser(String funct) {
        this.funct = funct;
    }

    public Func2XParser() {
        //To change body of created methods use File | Settings | File Templates.
    }

    public Func2X parser() {
        Func2X func2X = new Func2X();
        String[] token = funct.split("\\+");
        for (int i = 0; i < token.length; i++) {
            switch (countDelimiter(token[i])) {
                case 0: {
                    setFunc2XParam(1.0, token[i], func2X);
                    break;
                }
                case 1: {
                    String[] parseToken = token[i].split("\\*");
                    try {
                        setFunc2XParam(Double.valueOf(parseToken[0]), parseToken[1], func2X);
                    } catch (NumberFormatException e) {
                        setFunc2XParam(1.0, parseToken[0] + "*" + parseToken[1], func2X);
                    }


                    break;
                }
                case 2: {
                    String[] parseToken = token[i].split("\\*");
                    setFunc2XParam(
                            Double.valueOf(parseToken[0]),
                            (parseToken[1] + "*" + parseToken[2]),
                            func2X
                    );
                    break;
                }
                default: {
                    System.err.println("Bad delimiter in Func2XParser, parser()");
                }
            }
        }
        return func2X;
    }

    /*
    * Bad code
    */

    private int countDelimiter(String token) {
        int counter = 0;
        for (int i = 0; i < token.length(); i++) {
            if (token.charAt(i) == '*') {
                counter++;
            }
        }
        return counter;
    }

    private void setFunc2XParam(double value, String token, Func2X func2X) {
        if (token.equals("x1^2*x2^2")) {
            func2X.p2x12x2 += value;
        } else if (token.equals("x1^2*x2")) {
            func2X.p2x1x2 += value;
        } else if (token.equals("x2^2")) {
            func2X.p2x2 += value;
        } else if (token.equals("x1^2")) {
            func2X.p2x1 += value;
        } else if (token.equals("x1*x2")) {
            func2X.px1x2 += value;
        } else if (token.equals("x1*x2^2")) {
            func2X.px12x2 += value;
        } else if (token.equals("x1")) {
            func2X.px1 += value;
        } else if (token.equals("x2")) {
            func2X.px2 += value;
        } else {
            func2X.p += Double.valueOf(token);
        }

    }

    private boolean puseNotFirst(String[] token, boolean flag) {
        if (flag) {
            return !flag;
        } else {
            token[0] += "+";

            return flag;
        }
    }

    public String laTexFunct(Func2X func2X) {
        String []laTex = new String[1];
        laTex[0] = "";
        boolean first = true;
        if (func2X.p2x12x2 != 0.0) {
            first = puseNotFirst(laTex, first);
            laTex[0] += String.valueOf(func2X.p2x12x2) + "x_1^2x_2^2";
        }
        if (func2X.p2x1x2 != 0.0) {
            first = puseNotFirst(laTex, first);
            laTex[0] += String.valueOf(func2X.p2x1x2) + "x_1^2x_2";
        }
        if (func2X.px12x2 != 0.0) {
            first = puseNotFirst(laTex, first);
            laTex[0] += String.valueOf(func2X.px12x2) + "x_1x_2^2";
        }
        if (func2X.px1x2 != 0.0) {
            first = puseNotFirst(laTex, first);
            laTex[0] += String.valueOf(func2X.px1x2) + "x_1x_2";
        }
        if (func2X.p2x1 != 0.0) {
            first = puseNotFirst(laTex, first);
            laTex[0] += String.valueOf(func2X.p2x1) + "x_1^2";
        }
        if (func2X.p2x2 != 0.0) {
            first = puseNotFirst(laTex, first);
            laTex[0] += String.valueOf(func2X.p2x2) + "x_2^2";
        }
        if (func2X.px1 != 0.0) {
            first = puseNotFirst(laTex, first);
            laTex[0] += String.valueOf(func2X.px1) + "x_1";
        }
        if (func2X.px2 != 0.0) {
            first = puseNotFirst(laTex, first);
            laTex[0] += String.valueOf(func2X.px2) + "x_2";
        }
        if (func2X.p != 0.0) {
            first = puseNotFirst(laTex, first);
            laTex[0] += String.valueOf(func2X.p);
        }
        return laTex[0];
    }

}
