<!DOCTYPE html>
<html>
<head>
    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="./css/bootstrap-theme.min.css" rel="stylesheet">

    <link href="./css/main.css" rel="stylesheet">

    <link rel="stylesheet" href="./css/katex.min.css">
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.js"></script>
    <script src="http://cdn.kendostatic.com/2014.2.1008/js/kendo.all.min.js"></script>
    <script src="http://cdn.kendostatic.com/2014.2.1008/js/kendo.timezones.min.js"></script>


    <title></title>
</head>
<body>

<div class="bar">
    Levenberg API
</div>
<br/>


<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="input-group search">
                <input type="text" class="form-control" id="inputFunc" placeholder="3.4*x1^2*x2^2+3.0*x1*x2+3.4">
                <span class="input-group-btn">
                     <button class="btn btn-default" type="button" id="go">=</button>
                 </span>
            </div>
        </div>
    </div>
    <div class="row linkstyle">
        <div class="col-sm-8 col-sm-offset-2">
            <span id="more"><div class="glyphicon glyphicon-list"></div>
            more options</span>
        </div>
    </div>
</div>


<div class="container" id="morebox" style="display: none">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="panel panel-default">
                <!-- Default panel contents -->

                <div class="panel-body">
                    <p>Additional parameters</p>
                </div>
                <!-- input vector X={x1,x2} !-->
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-1">
                        <div class="input-group">
                            <span class="input-group-addon">X1</span>
                            <input type="text" class="form-control" id="x1" placeholder="0.0">
                        </div>
                        <br/>

                        <div class="input-group">
                            <span class="input-group-addon">X2</span>
                            <input type="text" class="form-control" id="x2" placeholder="0.0">
                        </div>
                    </div>

                    <div class="col-sm-7">

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="input-group">
                         <span class="input-group-addon">
                         <input type="checkbox" id="epsilon">
                         </span>
                                    <input type="text" class="form-control" id="inputParam"
                                           placeholder="step or count iteration">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                         <span class="input-group-addon">
                             h
                         </span>
                                    <input type="text" class="form-control" id="lavenH" placeholder="1">
                                </div>
                            </div>
                        </div>

                        <br/>
                        Enter your additional options. Click the checkbox to enter the number of iterations.
                    </div>
                </div>
                <br/>
            </div>
        </div>
    </div>
</div>


<br/>

<div class="container" id="result" style="display: none">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="panel panel-default">
                <!-- Default panel contents -->

                <!-- List group -->
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="title">Input</span>

                        <div id="input"></div>
                    </li>
                    <li class="list-group-item">
                        <span class="title">Gradient</span>

                        <div id="gradient1"></div>
                        <div id="gradient2"></div>
                    </li>
                    <li class="list-group-item">
                        <span class="title">Hesse</span>

                        <div id="hesse1"></div>
                        <div id="hesse2"></div>
                        <div id="hesse3"></div>
                        <div id="hesse4"></div>
                    </li>
                    <li class="list-group-item">
                        <span class="title">Computing</span>

                        <div id="computing"></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="./js/katex.min.js"></script>
<script src="./js/marked.js"></script>
<script>


    var defaultOption = false;
    var resultStatus = false;
    var urlLevenberg = '/solve';

    $("#more").click(function () {
        $("#morebox").toggle("slow", function () {
            defaultOption = !defaultOption;
        });
    });


    $("#go").click(function () {


        var inputFunc = document.getElementById('inputFunc').value;
        var inputParam = 0.0001;
        var dots = [0.0, 0.0];
        var activeEpsilon = true;
        var levenH = 1.0;
        if (defaultOption) {
            inputParam = document.getElementById('inputParam').value;
            dots = [document.getElementById('x2').value, document.getElementById('x2').value];
            activeEpsilon = document.getElementById('epsilon').checked;
            levenH = document.getElementById('lavenH').value;
        }
        var functional = {
            func: inputFunc,
            stopParam: inputParam,
            startDots: dots,
            epsilon: activeEpsilon,
            h: levenH
        }
        $.ajax({
            type: "POST",
            url: urlLevenberg,
            contentType: 'application/json',
            data: JSON.stringify(functional),
            success: function (e) {

                if (!resultStatus) {
                    $("#result").toggle("slow", function () {
                    });
                    resultStatus = true;
                }

                console.log(e);
                //Input data:
                input = e.input;
                if (input.epsilon) {
                    lableOption = "Epsilon";
                } else {
                    lableOption = "Iterations";
                }

                var inputString = marked('*= J(X) = ' + input.laTexFunc + ' =*');
                inputString += marked('*= X = ' +
                        '  (' + input.startDots[0] + ',' + input.startDots[1] + ');  ' +
                        '' + lableOption + ' : ' + input.stopParam + '; Step: ' + input.h + ' =*');
                document.getElementById('input').innerHTML = inputString;

                //Result data:
                result = e.result;
                //Gradient
                document.getElementById('gradient1').innerHTML =
                        marked('*=  {dJ \\over dx_1} = ' + result.grad[0] + ' =*');
                document.getElementById('gradient2').innerHTML =
                        marked('*=  {dJ \\over dx_2} = ' + result.grad[1] + ' =*');
                //Hesse
                document.getElementById('hesse1').innerHTML =
                        marked('*=  {d^2J \\over d^2x_1} = ' + result.hesse[0] + ' =*');
                document.getElementById('hesse2').innerHTML =
                        marked('*=  {d^2J \\over dx_1x_2} = ' + result.hesse[1] + ' =*');
                document.getElementById('hesse3').innerHTML =
                        marked('*=  {d^2J \\over dx_2x_1} = ' + result.hesse[2] + ' =*');
                document.getElementById('hesse4').innerHTML =
                        marked('*=  {d^2J \\over d^2x_2} = ' + result.hesse[3] + ' =*');


                var computing = "";
                for (i = 0; i < result.steps.length; i++) {

                    computing += marked('*= Iteration ' + (i + 1) + ': F(' + result.steps[i].dots[0] + ',' + result.steps[i].dots[1] + ') = ' + result.steps[i].fv + ' =*');
                }

                computing = marked('*= Result: F(' + result.steps[result.steps.length - 1].dots[0] + ',' + result.steps[result.steps.length - 1].dots[1] + ') = ' + result.steps[result.steps.length - 1].fv + ' =*') + computing;
                document.getElementById('computing').innerHTML = computing;

            }
        });


    })


</script>
</body>
</html>