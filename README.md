# О.
На данный момент репозиторий находится в состоянии сбора и оформления работ.

## Алгоритмы
[Ахо-корасик](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/Aho/?at=master) (2012)   
[Поиск в ширину](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/BreadthFirstSearch/?at=master) (2012)  
[BuildingHeap O(n)](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/BuildingHeap/?at=master) (2012)   
[Completing a tree](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd46/Algorithms/CompletingATree/?at=master) (2012)  
[Degree Array](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/DegreeArray/?at=master) (2012)     
[Double Degree Array](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/DoubleDegreeArray/src/DoubleDegreeArray.java?at=master) (2012)  
[Knapsack Problem](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/KnapsackProblem/?at=master) (2012)  
[Majority Element](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/MajorityElement/?at=master) (2012)  
[Нахождение точек бифуркации, мат.модели](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/MathModel/?at=master) (2012)  
[Merge Two Sorted Arrays](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/MergeTwoSortedArrays/?at=master) (2012)  
[Алгоритм Прима](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/Prim/?at=master) (2012)  
[Алгоритм Рабина-Карпа, Z-функция, КМП, Boyer-Moore](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/Strstr/src/Strstr.java?at=master) (2012)  
[2-Sum](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/Sum2/?at=master) (2012)  
[Tree Nonterm](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/TreeNonterm/?at=master) (2012)  
[Way Partition 2](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/WayPartition2/?at=master) (2012)  
[k-Statistic](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/median/?at=master) (2012)  
[3 Sum](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/Algorithms/sum3/?at=master) (2012)


[Задачи на Algo rosalind](http://algo.rosalind.info/users/ingvard)  
…
## Математика и Информатика
[Изучение принципов работы компилятора Milan и ввести в язык программирования операторы break и continue.](https://bitbucket.org/ingvard_/my-exercises/src/574a222df0d04c5efd198586b6a59e6259ccae1b/Milan-Simple/?at=master)    
## Программирование

### Java
[Ardroind game](https://bitbucket.org/ingvard_/my-exercises/src/acfcac69dd463141b87b099481a7ddc5e480344a/TowerDefense/?at=master) (2013)   
[Regular expression engine NFA](https://bitbucket.org/ingvard_/my-exercises/src/574a222df0d04c5efd198586b6a59e6259ccae1b/regExp/?at=master) JUnit (2013)  
[Шаблонизатор для html поддерживающий If,for](https://bitbucket.org/ingvard_/my-exercises/src/574a222df0d04c5efd198586b6a59e6259ccae1b/TemplateRegEx/?at=master)(2014)  
[Web-интерфейс для метода Левенберга](https://bitbucket.org/ingvard_/my-exercises/src/b7ffe44e1df7c783fe7b80a7801a476c90885bb8/Optimization-methods/?at=master) Spring MVC, JQuery, Bootstrap 3, Math (2014)

### С/С++

### Php

### Scala

### Тестирование


### Другое

### Применить в следующих проектах

***Heartbeat***   
Akka   
spark
[Преимущества и недостатки микросервисной архитектуры](http://eax.me/micro-service-architecture/)   
[Micro Service Architecture](https://yobriefca.se/blog/2013/04/28/micro-service-architecture/)