#2 Реализация
##2.1 Структура
	src
	|-- com
	|    |-- engine
	|    |	|-- BuilderNFA.java
	|    |	|-- FindInNFA.java
	|    |	|-- GraphBilder.java
	|    |	|-- PostfixParser.java
	|    |	|-- meet.java
	|    |	`-- sate.java
          |    |
	|    `-- example
	|	`-- main.java
	|
	|-- www
	|    |-- index.html
	|    |-- via.js
	|    `-- via.css
	|
	`-- test
	      ` java
	        `FindInNFATest.java

##2.2 Постфиксная форма (PostfixParser)
	
Для построения удобства построения NFA по регулярному выражению, было принято решение переводить в постфиксный формат. Алгоритм преобразования infix → postfix реализован классически на стеке, за некоторыми исключениям, связанными с тем, что регулярное выражение для некоторых операторов записано в постфиксной форме: '+' и '*'.  
**Пример:**  
Infix: ((a.)(m)+|(bc)*)+   
Postfix:  a.$#m$+_bc$*|$+  
где $ указывает на закрывающуюся скобку; # - на открывающуюся, сразу после закрывающийся; _ - символ пробела, для работы с оператором |

##2.3 Построение NFA (BuilderNFA)

Для построения NFA был реализован набор функций:

###zeroOrMore:
a*  
![image](https://bytebucket.org/ingvard_/my-exercises/raw/7427cec11bc430ad0f01c8ebfeae68b439fcad80/regExp/a%2A.png)

###oneOrMore:
a+  
![image](https://bytebucket.org/ingvard_/my-exercises/raw/7427cec11bc430ad0f01c8ebfeae68b439fcad80/regExp/a%2B.png)	

###alternation:
a|b  
![image](https://bytebucket.org/ingvard_/my-exercises/raw/7427cec11bc430ad0f01c8ebfeae68b439fcad80/regExp/alternation.png)	

##2.4 Построение графического представления NFA (GraphBilder)
Граф строится, модифицированным методом обхода в ширину.
Пример для выражения: ((abcd+)|(sda)+)*|12.4

![image](https://bytebucket.org/ingvard_/my-exercises/raw/7427cec11bc430ad0f01c8ebfeae68b439fcad80/regExp/all.png)	

##2.5 Поиск по регулярному выражению (FindInNFA)

Поиск по конечному автомату осуществляется при помощи модификации алгоритма обхода в глубину.

#3. Unti Test
```
package test.java;

import com.engine.BuilderNFA;
import com.engine.FindInNFA;
import com.engine.GraphBilder;
import com.engine.PostfixParser;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
public class FindInNFATest {
    private boolean tester(String regExp, String text) {
        PostfixParser parser = new PostfixParser(regExp);
        BuilderNFA builderNFA = new BuilderNFA(parser.parser());
        FindInNFA findInNFA = new FindInNFA(builderNFA.getRoot(), text);
        return findInNFA.find();
    }
    @Test
    public void testSimple() throws Exception {
        // True test    (RegExp, text)
        Assert.assertTrue(tester("text", "text"));
        Assert.assertTrue(tester("tex.", "text"));
        Assert.assertTrue(tester("12345", "12345"));
        Assert.assertTrue(tester("tex.t.z", "tex9t1z"));
        Assert.assertTrue(tester("(((a))(b)(c))", "abc"));
        Assert.assertTrue(tester("b(a(())(b)(c))()(b)b", "babcbb"));
        // False test
        Assert.assertFalse(tester("text", "texz"));
        Assert.assertFalse(tester("tex.", "texgt"));
        Assert.assertFalse(tester("12345", "123"));
        Assert.assertFalse(tester("tex.t.z", "tex921z"));
    }
    @Test
    public void zeroOrOne() throws Exception // ?
    {
    }
    @Test
    public void oneOrMore() throws Exception {  // +
        // True test    (RegExp, text)
        Assert.assertTrue(tester("a+", "a"));
        Assert.assertTrue(tester("a+", "aa"));
        Assert.assertTrue(tester("b+a+", "baa"));
        Assert.assertTrue(tester("b+a+", "bbbaa"));
        Assert.assertTrue(tester("b+a+", "bbba"));
        Assert.assertTrue(tester("b+a+", "ba"));
        Assert.assertTrue(tester("(ba)+", "ba"));
        Assert.assertTrue(tester("(ba)+", "baba"));
        Assert.assertTrue(tester("(ba)+(cd)+", "bacd"));
        Assert.assertTrue(tester("(ba)+(cd)+", "babacdcdcd"));
        Assert.assertTrue(tester("(ba)+(cd)+a", "bacdcda"));
        Assert.assertTrue(tester("((ba)+(cd)+)+", "bacd"));
        Assert.assertTrue(tester("(a+b+)+", "aabbbaaabb"));
       // Assert.assertTrue(tester("((a)+(b)+)+", "aabbbaaabb"));
        // False test
        Assert.assertFalse(tester("a+", ""));
        Assert.assertFalse(tester("a+", "ba"));
        Assert.assertFalse(tester("a+", "ab"));
        Assert.assertFalse(tester("a+", "aab"));
        Assert.assertFalse(tester("a+", "aba"));
        Assert.assertFalse(tester("a+b+", "ba"));
        Assert.assertFalse(tester("a+", "aaabba"));
        Assert.assertFalse(tester("a+b+", ""));
    }
    @Test
    public void zeroOrMore()throws Exception {  // *
    }
    @Test
    public void alternation()throws Exception {  // |
        // True test    (RegExp, text)
        Assert.assertTrue(tester("a|b", "a"));
        Assert.assertTrue(tester("a|b", "b"));
        Assert.assertTrue(tester("(ab)|b", "b"));
        Assert.assertTrue(tester("(ab)|b", "ab"));
        Assert.assertTrue(tester("(a)|(b)", "a"));
        Assert.assertTrue(tester("(a+)|(b+)", "aaaaa"));
        Assert.assertTrue(tester("(a+)|(b+)", "bb"));
        Assert.assertTrue(tester("a+|b+", "a"));
        Assert.assertTrue(tester("a+|b+", "bbb"));
       // Assert.assertTrue(tester("(a|b)+", "abbbaa")); // this is misstake
        // False test
        Assert.assertFalse(tester("...", "1234"));
    }
    @Test
    public void catenation()throws Exception {  // .
        // True test    (RegExp, text)
        Assert.assertTrue(tester("...", "123"));
        Assert.assertTrue(tester("1.", "14"));
        Assert.assertTrue(tester(".3", "43"));
        Assert.assertTrue(tester(".2.", "324"));
        Assert.assertTrue(tester(".", "a"));
        // False test
        Assert.assertFalse(tester("...", "1234"));
        Assert.assertFalse(tester(".1", "12"));
        Assert.assertFalse(tester("1.", "21"));
        Assert.assertFalse(tester(".2.", "111"));

    }

}
```
#Вывод
В ходе разработки алгоритма была допущена ошибка в преобразовании в постфиксный формат, что привело к неоднозначности распознавания некоторых выражений. Ошибка была обнаружена на последнем этапе, когда производилось покрытие Unit Test. В тестах представлены рабочие примеры.	

#Литература
[1] - http://swtch.com/~rsc/regexp/regexp1.html