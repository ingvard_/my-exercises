package com.engine;

import java.util.ArrayDeque;

/**
 * Created by Maxim on 6/10/2014.
 */
public class BuilderNFA {

    private ArrayDeque<meet> stackState;


    public meet merge(meet left, meet right) {
        meet newMeet = new meet();

        newMeet.start = left.start;
        newMeet.end = right.end;

        left.end.left = right.start;
        right.start.prev = left.end;

        return newMeet;
    }


    public meet zeroOrOne(meet branch) {
       /* branch.end.left = new state();
        branch.end.left.prev = branch.end;
        branch.end = branch.end.left;
        branch.end.label = '?';
        /**
         * Рассмотрим 2 возможные ситуации:
         * 1. 1 - lable+
         * 2. More (lable  lable lable lable)+
         */
       /* if (branch.end.prev.label != '$') { // Повторить 1 букву
            branch.end.right = branch.end.prev;
        } else {
            state cursor = branch.end.prev;
            while (true) {
                if (cursor.prev == null || cursor.prev.label == '#') {
                    branch.end.right = cursor;
                    break;
                }
                cursor = cursor.prev;
            }
        }     */
        return branch;
    }

    public meet oneOrMore(meet branch) {
        branch.end.left = new state();
        branch.end.left.prev = branch.end;
        branch.end = branch.end.left;
        branch.end.label = '+';
        int bracket = 0;
        /**
         * Рассмотрим 2 возможные ситуации:
         * 1. 1 - lable+
         * 2. More (lable  lable lable lable)+
         *
         *    a$+#b$+$+
                a+b+$+
         */
        if (branch.end.prev.label != '$') { // Повторить 1 букву
            branch.end.right = branch.end.prev;
        } else {
            state cursor = branch.end.prev;
            while (true) {
                /*if(!cursor.equals(branch.start)) {
                    if (cursor.prev.label == '$' && cursor.label == '+') {
                        bracket++;
                    }
                } */
                if (cursor.prev == null || cursor.prev.label == '#') {
                            branch.end.right = cursor;
                            break;
                }
                cursor = cursor.prev;
            }
        }
        return branch;
    }

    public meet zeroOrMore(meet branch) {

        branch.end.left = new state();
        branch.end.left.prev = branch.end;
        branch.end = branch.end.left;
        branch.end.label = '*';
        /**
         * Рассмотрим 2 возможные ситуации:
         * 1. 1 - lable*
         * 2. More (lable  lable lable lable)*
         */
        if (branch.end.prev.label != '$') { // Повторить 1 букву
            branch.end.right = branch.end.prev;
            branch.end.prev.prev.right = branch.end;
        } else {
            state cursor = branch.end.prev;
            while (true) {
                if (cursor.prev == null) {
                    cursor.right = branch.end;
                    branch.end.right = cursor;
                    break;
                } else if (cursor.prev.label == '#') {
                    cursor.prev.right = branch.end;
                    branch.end.right = cursor;
                    break;
                }
                cursor = cursor.prev;
            }
        }
        return branch;
    }

    public meet alternation(meet fork1, meet fork2) {
        meet newBranch = new meet();
        newBranch.start = new state();
        newBranch.start.label = '|';
        newBranch.end = new state();
        newBranch.end.prev = newBranch.start;
        // connect
        newBranch.start.left = fork1.start;
        newBranch.start.right = fork2.start;
        fork1.start.prev = newBranch.start;
        fork2.start.prev = newBranch.start;
        fork1.end.left = newBranch.end;
        fork2.end.left = newBranch.end;
        return newBranch;
    }

    public meet emptyItem() {
        state root = new state();
        meet firstMeet = new meet();
        firstMeet.start = root;
        firstMeet.end = root;
        return firstMeet;
    }

    public BuilderNFA(String posfix) {
        stackState = new ArrayDeque<meet>();
        stackState.push(emptyItem());

        for (int i = 0; i < posfix.length(); i++) {
            switch (posfix.charAt(i)) {
                case '+': {
                    ///
                    /* if(stackState.size() == 2) {
                         meet fork2 = stackState.pop();
                         meet fork1 = stackState.pop();
                         stackState.push(merge(fork1, fork2));
                     }                                   */

                    //
                    stackState.push(oneOrMore(stackState.pop()));
                    break;
                }
                case '*': {
                    stackState.push((zeroOrMore(stackState.pop())));
                    break;
                }

                case '?': {
                    stackState.push((zeroOrOne(stackState.pop())));
                    break;
                }

                case '|': {
                    meet fork2 = stackState.pop();
                    meet fork1 = stackState.pop();
                    stackState.push(alternation(fork1, fork2));
                }
                case ' ': {
                    stackState.push(emptyItem());
                    break;
                }
                case '#': {

                    stackState.push(emptyItem());
                    break;
                }


                default: {

                   // if (stackState.peek().start.label == '\u0000') {
                   //     stackState.peek().start.label = posfix.charAt(i);
                   // } else {
                        meet temp = new meet();
                        temp.start = new state();
                        temp.end = temp.start;
                        temp.start.label = posfix.charAt(i);
                        stackState.push(merge(stackState.pop(), temp));

                   // }

                }
            }
        }
        while (stackState.size() > 1) {
            meet fork2 = stackState.pop();
            meet fork1 = stackState.pop();
            stackState.push(merge(fork1, fork2));
        }
    }

    public meet getRoot() {
        return stackState.peek();
    }
}
