package com.engine;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by Maxim on 6/10/2014.
 */
public class GraphBilder {

    private HashSet<state> visit;
    private Queue<state> queBFS;
    private ArrayList<vertex> jsonState;

    class vertex {
        public String name;
        public ArrayList<String> depends;

        vertex() {
            name = new String();
            depends = new ArrayList<String>();
        }

        public void addDepends(String nameDepend) {
            depends.add(nameDepend);
        }
    }

    public String vertexPrinter(vertex items) {
        String newVertex = "";

        for (int j = 0; j < items.depends.size(); j++) {
            newVertex += "\"" + items.name + "\"";
            newVertex += " -> \"" + items.depends.get(j) + "\"";

            newVertex += ";";

        }
        return newVertex;

    }


    public GraphBilder(meet root, String src) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(src));

        visit = new HashSet<state>();
        jsonState = new ArrayList<vertex>();
        queBFS = new LinkedList<state>();
        // BFS
        queBFS.add(root.start);
        state currentState;
        while (!queBFS.isEmpty()) {
            currentState = queBFS.remove();
            if (!visit.contains(currentState)) {
                visit.add(currentState);

                if (currentState.left != null) {
                    queBFS.add(currentState.left);
                }
                if (currentState.right != null) {
                    queBFS.add(currentState.right);
                }
            }
        }
        // Init vertex array for gen Json
        Iterator<state> itr = visit.iterator();

        for (int i = 0; itr.hasNext(); i++) {
            vertex newVertex = new vertex();
            state temp = itr.next();
            if (temp.label != '\u0000') {
                newVertex.name = temp.label + "_" + temp.toString();
            } else {
                newVertex.name = "Null_" + temp.toString();
            }

            if (temp.left != null) {
                if (temp.left.label != '\u0000') {
                    newVertex.addDepends(temp.left.label + "_" + temp.left.toString());
                } else {
                    newVertex.addDepends("Null_" + temp.left.toString());
                }

            }
            if (temp.right != null) {
                if (temp.right.label != '\u0000') {
                    newVertex.addDepends(temp.right.label + "_" + temp.right.toString());
                } else {
                    newVertex.addDepends("Null_" + temp.right.toString());
                }
            }
            jsonState.add(newVertex);

        }


        writer.write("<html>\n" +
                "<head>\n" +
                "  <title>NFA</title>\n" +
                "\n" +
                "  <script type=\"text/javascript\" src=\"./vis.js\"></script>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div id=\"mygraph\"></div>\n" +
                "\n" +
                "<script type=\"text/javascript\">\n" +
                "  var container = document.getElementById('mygraph');\n" +
                "  var data = {\n" +
                "    dot: 'digraph {node[shape=box];");

        for (int i = 0; i < jsonState.size(); i++) {
            String printer = vertexPrinter(jsonState.get(i));
            writer.write(printer);
        }

        writer.write("}'\n" +
                "  };\n" +
                "  var graph = new vis.Graph(container, data);\n" +
                "</script>\n" +
                "</body>\n" +
                "</html>");

        writer.close();
    }
}
