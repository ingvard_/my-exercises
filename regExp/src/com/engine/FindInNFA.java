package com.engine;
/**
 * Created by Maxim on 6/11/2014.
 */
public class FindInNFA {
    private String text;
    private meet root;
    private int len;
    private boolean find;

    public FindInNFA(meet root, String text) {
        this.find = false;
        this.text = text;
        this.root = root;
        this.len = text.length();
    }

    public void DFSFind(state current, int posInString) {
        // if find == true that we be in finish state
        if (find == false) {



            if (current.equals(root.end)) {
                if ((posInString+1) == len && (current.label == text.charAt(posInString) || current.label == '.'))
                {
                    find = true;
                    return;
                }
                //|| current.label == '+'
                // Переделать
                else if(current.label == '*' || current.label == '+'){
                    /*if (current.left != null && current.left.label == text.charAt(posInString)){
                        find = true;
                        return;
                    }               */
                    if(posInString == len)
                    {
                        find = true;
                        return;
                    }
                    if (current.right != null && current.right.label == text.charAt(posInString) && (posInString+1) == len){
                        find = true;
                        return;
                    }

                }
                else if(current.label == '\u0000' || current.label == '$'  || current.label == '#'){
                    if (current.left == null && posInString == len){
                        find = true;
                        return;
                    }

                }
            }



            if (current.label == '\u0000' || current.label == '$' || current.label == '*' || current.label == '#' || current.label == '|' || current.label == '+') {
                ///
                if (current.left != null){
                    DFSFind(current.left, posInString);


                }

                if (current.right != null)
                    DFSFind(current.right, posInString);
            } else {
                // if len == posIn
                if (posInString == len) {
                    return;
                }

                switch (current.label) {
                    case '.': {
                        if (current.left != null)
                            DFSFind(current.left, posInString + 1);
                        if (current.right != null)
                            DFSFind(current.right, posInString + 1);
                        break;
                    }
                    default: {
                        if (current.label == text.charAt(posInString)) {
                            if (current.left != null)
                                DFSFind(current.left, posInString + 1);
                            if (current.right != null)
                                DFSFind(current.right, posInString + 1);

                        }
                    }
                }
            }
        }
    }


    public boolean find() {
        DFSFind(root.start, 0);
        return find;

    }
}
