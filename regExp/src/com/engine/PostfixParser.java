package com.engine;
import java.util.ArrayDeque;

/**
 * Created by Maxim on 6/10/2014.
 */
public class PostfixParser {
    private ArrayDeque<Character> stack;
    private String postfixForm;

    public PostfixParser(String infix) {
        postfixForm = new String("");
        stack = new ArrayDeque<Character>();
        for (int i = 0; i < infix.length(); i++) {
            if (infix.charAt(i) == '|' || infix.charAt(i) == '(') {
                stack.push(infix.charAt(i));
                if (infix.charAt(i) == '|') {
                    //spike for pleasant Syntex: "$ " => " "
                    if (postfixForm.charAt(postfixForm.length() - 1) == '$') {
                        postfixForm = postfixForm.substring(0, postfixForm.length() - 1);
                    }
                    postfixForm += " ";
                }
                //spike for situation: expression(expression)
                if (i > 0 && infix.charAt(i) == '(' && infix.charAt(i - 1) != '(' && infix.charAt(i - 1) != '|') {
                    postfixForm += "#";
                }
            } else if (infix.charAt(i) == ')') {
                while (stack.size() > 0) {
                    if (stack.peek() != '(') {
                        //spike for pleasant Syntex: "$|" => "|"
                        if(stack.peek() == '|' && postfixForm.charAt(postfixForm.length()-1) == '$') {
                            postfixForm = postfixForm.substring(0, postfixForm.length() - 1);
                        }
                        postfixForm += stack.pop();
                    } else {

                        postfixForm += "$";
                        stack.pop();
                        break;
                    }
                }
            } else {
                postfixForm += infix.charAt(i);
            }
        }
        while (stack.size() > 0) {
            //spike for pleasant Syntex: "$|" => "|"
            if (stack.peek() == '|' && postfixForm.charAt(postfixForm.length()-1) == '$') {
                postfixForm = postfixForm.substring(0, postfixForm.length() - 1);
            }
            postfixForm += stack.pop();
        }
    }

    public String parser() {
        return postfixForm;
    }

}
