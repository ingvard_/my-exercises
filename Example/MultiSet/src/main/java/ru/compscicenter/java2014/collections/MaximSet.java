package ru.compscicenter.java2014.collections;

import java.util.AbstractCollection;

import java.util.Iterator;

/**
 * Created by maksim on 30.03.15.
 */
/**
 * MultiSet is an unordered collection that may contain duplicate elements.
 * It refines some of the operations inherited from {@link java.util.Collection}
 * and adds several new operations aware of MultiSet's ability to contain multiple
 * occurrences of an element.
 *
 * Implementations must provide default constructor (without arguments)
 * and constructor with single argument of type <code>Collection&lt;? extends E&gt;</code>.
 *
 * @param <E> the type of elements in this multiset
 */

public class MaximSet<E> extends AbstractCollection<E> implements MultiSet<E>  {

    /**
     * Returns an iterator over the elements contained in this collection.
     *
     * @return an iterator over the elements contained in this collection
     */
    @Override
    public Iterator iterator() {
        return null;
    }

    /**
     * Adds multiple occurrences of the specified element to this multiset.
     *
     * @param e           element to add
     * @param occurrences number of element occurrences to add; can't be negative
     * @return the count of element occurrences before the operation; possibly zero
     * @throws IllegalArgumentException if <code>occurrences</code> is negative
     */
    @Override
    public int add(E e, int occurrences) {
        return 0;
    }

    /**
     * Removes multiple occurrences of the specified element from this multiset, if present.
     * If multiset contains fewer copies of the element than given by <code>occurrences</code>
     * parameter, all occurrences are removed.
     *
     * @param e           element to remove
     * @param occurrences number of element occurrences to remove; can't be negative
     * @return the count of element occurrences before the operation; possibly zero
     * @throws IllegalArgumentException if <code>occurrences</code> is negative
     */
    @Override
    public int remove(Object e, int occurrences) {
        return 0;
    }

    /**
     * Returns the number of occurrences of an element in this multiset,
     * or <code>0</code> if multiset does not contain this element.
     *
     * @param e the element to whose occurrences are to be returned
     * @return the number of occurrences of an element in this multiset
     */
    @Override
    public int count(Object e) {
        return 0;
    }

    @Override
    public int size() {
        return 0;
    }
}
