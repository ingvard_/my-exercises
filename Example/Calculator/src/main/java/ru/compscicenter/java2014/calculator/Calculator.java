package ru.compscicenter.java2014.calculator;

/**
 * Created by maksim on 29.03.15.
 */
public class Calculator {

    private double lastResult;
    private String lastExpr;

    public Calculator(String expression){
        calculate(expression);
    }

    public Calculator(){
        this.lastExpr = "";
        this.lastResult = 0.0;
    }

    public double calculate(String expression){
        return 0.0;
    }

    public double getLastRes(){
         return lastResult;
    }

    public String getLastExpr(){
          return lastExpr;
    }


}
