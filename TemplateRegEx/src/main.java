import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.*;
import java.util.regex.*;

/**
 * Created with IntelliJ IDEA.
 * User: Maxim
 * Date: 5/26/14
 * Time: 11:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class main {

    private static Map<String, variable> variableList;
    private static Map<String, String> userTag;
    private static String generationDocument = "";
    private static int writeFlow = 0;

    private static ArrayList<String> AccumulatorFor = new ArrayList<String>();
    private static int countForOperation = 0;
    private static int forStarted = 0;

    private static int countEndFor = 0;


    private static class variable {
        public ArrayList<String> list;

        variable(String varList) {
            list = new ArrayList<String>();
            //RegExp for parse variable
            Pattern pattern = Pattern.compile("(([A-Za-z0-9]|\\s)+)");
            Matcher matcher = pattern.matcher(varList);
            while (matcher.find()) {
                // Get the matching string
                String match = matcher.group();
                list.add(match);
            }
        }
    }

    // Converts the contents of a file into a CharSequence
    public static CharSequence fromFile(String filename) throws IOException {
        FileInputStream input = new FileInputStream(filename);
        FileChannel channel = input.getChannel();

        // Create a read-only CharBuffer on the file
        ByteBuffer bbuf = channel.map(FileChannel.MapMode.READ_ONLY, 0, (int) channel.size());
        CharBuffer cbuf = Charset.forName("8859_1").newDecoder().decode(bbuf);
        return cbuf;
    }

    public static void loadUserTag(String src) throws IOException {
        System.out.println("Loading userTag...");
        BufferedReader reader = new BufferedReader(new FileReader(src));
        // Parse userTag
        //  \{[A-Za-z0-9]+}
        // \\s modificator [ \f\n\r\t\v]
        Pattern pattern = Pattern.compile("(\\{[A-Za-z0-9]+}(.*)=(.*)(\\{([^\\{])*)(};))");
        Pattern nameTag = Pattern.compile("([A-Za-z0-9]+)");
        Pattern allCode = Pattern.compile("(.*\\s)*");
        Matcher matcher = pattern.matcher(fromFile(src));
        System.out.println("Tag:");
        // Find all matches
        int i = 0;
        while (matcher.find()) {
            // Get the matching string
            String match = matcher.group();
            String[] token = new String[2];
            token[0] = match.substring(0, match.indexOf("=") - 1);
            token[1] = match.substring(match.indexOf("=") + 1, match.length());
            Matcher test = nameTag.matcher(token[0]);
            test.find();
            String nameUserTag = test.group();
            //code for substitution
            test = allCode.matcher(token[1]);
            test.find();
            String code = (test).group();
            code = code.substring(code.indexOf("{") + 1, code.length());
            userTag.put(nameUserTag, code);
            System.out.println("------ " + nameUserTag);
        }
        System.out.println("Tag uploaded.");

    }


    private static int tagOrVar(String input) {

        if (variableList.containsKey(input)) {
            return 1;
        }
        if (userTag.containsKey(input)) {
            return 2;
        }

        System.out.println("Error " + input + ". It does not exist.");

        return -1;

    }

    private static int recognizer(String code) {
        Pattern forStart = Pattern.compile("\\{for=[0-9]+}");
        if (forStart.matcher(code).matches())
            return 1;
        Pattern forEnd = Pattern.compile("\\{EndFor}");
        if (forEnd.matcher(code).matches())
            return 2;
        Pattern ifStart = Pattern.compile("\\{if\\([A-Za-z0-9]+\\):\\[([0-9]+|,)+\\]}");
        if (ifStart.matcher(code).matches())
            return 3;
        Pattern ifEnd = Pattern.compile("\\{EndIf}");
        if (ifEnd.matcher(code).matches())
            return 4;
        Pattern newVarible = Pattern.compile("(\\{[A-Za-z0-9]+:.*})");
        if (newVarible.matcher(code).matches())          // New Varible
            return 5;
        Pattern indexFor = Pattern.compile("(@id[0-9]+)");
        if (indexFor.matcher(code).matches())
            return 6;
        Pattern pattern = Pattern.compile("(\\{.*})"); // I's varible or user Tag
        if (pattern.matcher(code).matches())
            return 7;
        return -1;
    }

    private static String valueTag(String comand) { // input command: {nameVar} or {nameVar[Index]}

        Pattern arrayVar = Pattern.compile("\\{[A-Za-z0-9]+\\[[0-9]+\\]}");
        if (arrayVar.matcher(comand).matches()) {
            Pattern arrayIndex = Pattern.compile("[0-9]+");
            Matcher matcherIndex = arrayIndex.matcher(comand);
            if (matcherIndex.find()) {
                Pattern nameVar = Pattern.compile("[A-Za-z0-9]+");
                Matcher name = nameVar.matcher(comand);
                name.find();

                variable temp = variableList.get(name.group());
                // System.out.println("Index: "+Integer.valueOf(matcherIndex.group()));
                return (temp.list.get(Integer.valueOf(matcherIndex.group())));

            } else {
                System.out.println("Ops error array block");
            }

        } else {
            // Print the contents of the variable

            switch (tagOrVar(comand.substring(1, comand.length() - 1))) {
                case 1: {
                    variable temp = variableList.get(comand.substring(1, comand.length() - 1));
                    return (temp.list.get(0));

                }
                case 2: {

                    return (userTag.get(comand.substring(1, comand.length() - 1)));

                }
                default:
                    return ("Error block var");

            }

        }
        return null;


    }

    private static void engineInterpreter(String interpretLine,int nesting) {
        Pattern pattern = Pattern.compile("(\\{.*})");

        Matcher matcher = pattern.matcher(interpretLine);
        int interpretPosition = 0;
        if (forStarted == 0) {
            while (matcher.find()) {
                String match = matcher.group();
                //System.out.println("----"+match);


                switch (recognizer(match)) {
                    case 5: {
                        // Push new variable in Table
                        String value = (match.split(":"))[1];
                        variableList.put((match.split(":"))[0].substring(1), new variable(value.substring(0, value.length() - 1)));
                        System.out.println("add new ver: " + match);
                        if (writeFlow == 0) {
                            generationDocument += interpretLine.substring(interpretPosition, matcher.start());
                            interpretPosition = matcher.end();
                        }
                        break;
                    }
                    case 7: {

                        if (writeFlow == 0) {
                            generationDocument += interpretLine.substring(interpretPosition, matcher.start()) + valueTag(match);
                            interpretPosition = matcher.end();
                        }

                        System.out.println(valueTag(match));
                        break;

                    }
                    case 1: {
                        Pattern countIneration = Pattern.compile("[0-9]+");
                        Matcher Iteration = countIneration.matcher(match);
                        Iteration.find();

                        System.out.println("count iteration: " + Iteration.group());
                        countForOperation = Integer.valueOf(Iteration.group());
                        forStarted = 1;
                        writeFlow = 1;

                        break;
                    }
                    case 2: {

                        break;
                    }

                    case 3: {   //Start If

                        Pattern ifIndeficator = Pattern.compile("[A-Za-z0-9]+");
                        Matcher Indeficator = ifIndeficator.matcher(match);
                        Indeficator.find();  // first eat if
                        Indeficator.find();  // second indeficator


                        Pattern SetAdmissibleValues = Pattern.compile("\\[([0-9]+|,)+]");
                        Matcher SetAdmissible = SetAdmissibleValues.matcher(match);
                        SetAdmissible.find();

                        String[] token = (SetAdmissible.group().substring(1, SetAdmissible.group().length() - 1)).split(",");
                        int flag = 0;
                        for (String items : token) {
                            if (items.equals(valueTag("{" + Indeficator.group() + "}"))) {
                                flag = 1;
                            }
                        }
                        if (flag == 0) {
                            writeFlow = 1;
                        }
                        interpretPosition = interpretLine.length();
                        break;

                    }
                    case 4: { // EndIf

                        writeFlow = 0;
                        interpretPosition = interpretLine.length();
                    }
                    default:
                        // System.out.print("System error!");
                }


                //System.out.println(generationDocument);


            }
        } else {
            String match = "";
            if(matcher.find()) {
                match = matcher.group();
            }
            if(recognizer(match) == 1)
            {
                countEndFor++;
            }
            if (recognizer(match) == 2) {
                if(countEndFor == 0){
                forStarted = 0;
                writeFlow = 0;
                ArrayList<String> temp = new ArrayList<String>();
                    for(int j=0; j < AccumulatorFor.size();j++) {
                        temp.add(AccumulatorFor.get(j));
                    }
                AccumulatorFor.clear();
                int countOpr = countForOperation;
                for(int i = 0 ; i < countOpr; i++){
                    Pattern findId = Pattern.compile("@id"+nesting);

                    int iter = i+1;
                    for(int j=0; j < temp.size();j++) {


                        if (Pattern.matches(".*@id"+nesting+".*", temp.get(j))){
                            Matcher findIdMatcher = findId.matcher(temp.get(j));
                            String newString = "";
                            int pos=0;
                            while(findIdMatcher.find()){
                                newString += temp.get(j).substring(pos,findIdMatcher.start())+String.valueOf(iter);
                                pos = findIdMatcher.end();
                            }
                            newString += temp.get(j).substring(pos,temp.get(j).length());
                            engineInterpreter(newString, nesting + 1);
                        }
                        else{
                            engineInterpreter(temp.get(j), nesting + 1);
                        }

                    }
                }

                interpretLine = "";
                AccumulatorFor.clear();
                }
                else{
                    countEndFor--;
                    AccumulatorFor.add(interpretLine);
                }
            } else {
                AccumulatorFor.add(interpretLine);
            }
        }
        if (writeFlow == 0)
            if(interpretLine.equals("")){

            }
        else {
            generationDocument += interpretLine.substring(interpretPosition, interpretLine.length()) + "\n";
            }

    }


    public static void main(String[] arg) throws IOException {
        System.out.println("Select: [1 - read from a file; 2 - With Console]");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();



        variableList = new HashMap();
        userTag = new HashMap();

       // BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\Maxim\\Desktop\\Учёба в 6 семестре\\Компиляторы\\TemplateRegEx\\data\\simpleCode.templ"));
        BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\Maxim\\Desktop\\Учёба в 6 семестре\\Компиляторы\\TemplateRegEx\\data\\html.templ"));
        loadUserTag("C:\\Users\\Maxim\\Desktop\\Учёба в 6 семестре\\Компиляторы\\TemplateRegEx\\data\\UserTag.templ");


        String interpretLine;
        if(s.equals("1")){
            while ((interpretLine = reader.readLine()) != null) {
                engineInterpreter(interpretLine,0);
            }
        } else
        {
            while ((s = br.readLine()) != null) {
                if(s.equals("exit")){
                    break;
                }
                engineInterpreter(s,0);
            }
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter("C:\\Users\\Maxim\\Desktop\\Учёба в 6 семестре\\Компиляторы\\TemplateRegEx\\data\\result.html"));
        writer.write(generationDocument);
        System.out.println(generationDocument);
        writer.close();

    }
}
