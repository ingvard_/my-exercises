#Содержание
Введение  
2 Синтаксис  
2.1 Переменные  
2.2 Условия If  
2.3 Цикл For  
3 Реализация  
3.1 Разбор выражений  
3.2 Переменные и теги  
3.3 Ветвление if  
3.4 Цикл for  
4 Пример работы  
4.1 Генерация Html-шаблона  
4.2 Абстрактная программа  
Вывод  
Литература  

#Введение
Цель: создать шаблонизатор, который взаимодействует с HTML и автоматизирует однотипные операции. Интерпретатор может выполнять команды из консоли ввода. 

В работе демонстрируется не самый лучший подход, выполненный в обход синтаксическому анализатору.:)

#2 Синтаксис
##2.1 Переменные
**Переменные в языке делятся на два типа:**

1. Заранее подготовленные пользовательские теги. Описанные в специальном файле UserTag.templ. Объявление имеет следующий синтаксис: 

```
{Имя тега} = { Html-код или текст
}; 
```
Подстановка значения тега происходит автоматически.  
** Пример: **

```
Содержимое файла c тегами: {userTag} = { <html> text </html>
};
>>{userTag}
<html> text </html>
```

2. Переменные, создаваемые в ходе интерпретации программы, имеют следующий синтаксис:

**{ Имя переменной : значение }** или **{ Имя переменной : 'значение' }** 

Объявить массив можно следующим образом :  
 **{ Имя массива : [ Кортеж значений, через запятую ] }**
		
В ходе работы переменная может быть переопределена, как массив и 		наоборот. Как и в ситуации с тегами, обратиться к переменной можно следующим образом:  
	**{Имя переменной} или { Имя массива [Индекс, начиная с 0] }**
	
В качестве индекса или присваивания может выступать и переменная, но только особого типа **@id***
** Пример: **

```
>>{name:”Bob”}
>>{name}
Bob
>>{array:[”Sara”,”Kira”,”Lex”,25]}
>>{array[1]}
Kira
```
##2.2 Условия If
Условный оператор имеет следующий синтаксис:  

```
{if(Имя переменной):[Кортеж доступных значений, через запятую]}
                Вставка Html кода, условные операторы, циклы;
                        …...
        {EndIf}
```
** Замечание: **
	Если необходимо использовать в качестве индикатора динамически меняющиеся значения:
	
```
{test:@id0}
        {if(test):[5,6,7,3]}
        …..
```
##2.3 Цикл For
Данный язык поддерживает возможность вложенных циклов. Для вывода номера итерации используется специальный символ @id_(Число вложенности).

```
{for=Количество итераций} // 0 цикл
        {for=Количество итераций} // цикл 1
                @id0 – выведет на какой итерации находится цикл 0.
                @id1 – выведет на какой итерации находится цикл 1.
        {EndFor}
{EndFor}
```

#3 Реализация
##3.1 Разбор выражений
Для разбора выражений языка используются регулярные выражения:

```
private static int recognizer(String code) {
        Pattern forStart = Pattern.compile("\\{for=[0-9]+}");
        if (forStart.matcher(code).matches())
            return 1;
        Pattern forEnd = Pattern.compile("\\{EndFor}");
        if (forEnd.matcher(code).matches())
            return 2;
        Pattern ifStart = Pattern.compile("\\{if\\([A-Za-z0-9]+\\):\\[([0-9]+|,)+\\]}");
        if (ifStart.matcher(code).matches())
            return 3;
        Pattern ifEnd = Pattern.compile("\\{EndIf}");
        if (ifEnd.matcher(code).matches())
            return 4;
        Pattern newVarible = Pattern.compile("(\\{[A-Za-z0-9]+:.*})");
        if (newVarible.matcher(code).matches())          // New Varible
            return 5;
        Pattern indexFor = Pattern.compile("(@id[0-9]+)");
        if (indexFor.matcher(code).matches())
            return 6;
        Pattern pattern = Pattern.compile("(\\{.*})"); // I's varible or user Tag
        if (pattern.matcher(code).matches())
            return 7;
        return -1;
    }
```

При совпадении выполняется блок, отвечающий за данное действие.

##3.2 Переменные и теги
Все переменные и теги хранятся в соответствующих Hash-таблицах в определённой структуре:

```
 private static Map<String, variable> variableList;
 private static Map<String, String> userTag;
 private static class variable {
        public ArrayList<String> list;

        variable(String varList) {
            list = new ArrayList<String>();
            //RegExp for parse variable
            Pattern pattern = Pattern.compile("(([A-Za-z0-9]|\\s)+)");
            Matcher matcher = pattern.matcher(varList);
            while (matcher.find()) {
                // Get the matching string
                String match = matcher.group();
                list.add(match);
            }
        }
    }
```

##3.3 Ветвление if
```
	case 3: {   //Start If

                        Pattern ifIndeficator = Pattern.compile("[A-Za-z0-9]+");
                        Matcher Indeficator = ifIndeficator.matcher(match);
                        Indeficator.find();  // first eat if
                        Indeficator.find();  // second indeficator


                        Pattern SetAdmissibleValues = Pattern.compile("\\[([0-9]+|,)+]");
                        Matcher SetAdmissible = SetAdmissibleValues.matcher(match);
                        SetAdmissible.find();

                        String[] token = (SetAdmissible.group().substring(1, SetAdmissible.group().length() - 1)).split(",");
                        int flag = 0;
                        for (String items : token) {
                            if (items.equals(valueTag("{" + Indeficator.group() + "}"))) {
                                flag = 1;
                            }
                        }
                        if (flag == 0) {
                            writeFlow = 1;
                        }
                        interpretPosition = interpretLine.length();
                        break;

                    }
                    case 4: { // EndIf

                        writeFlow = 0;
                        interpretPosition = interpretLine.length();
                    }
```

Конструкция If имеет следующее поведение:  
1. При распознавании выражения и корректности условия происходит печать содержимого тела конструкции.  
2. В противном случае  перекрываем поток генерации кода до чтения тега {EndIf}, т.е просто игнорируем код в теле.

##3.4 Цикл for
```
	String match = "";
            if(matcher.find()) {
                match = matcher.group();
            }
            if(recognizer(match) == 1)
            {
                countEndFor++;
            }
            if (recognizer(match) == 2) {
                if(countEndFor == 0){
                forStarted = 0;
                writeFlow = 0;
                ArrayList<String> temp = new ArrayList<String>();
                    for(int j=0; j < AccumulatorFor.size();j++) {
                        temp.add(AccumulatorFor.get(j));
                    }
                AccumulatorFor.clear();
                int countOpr = countForOperation;
                for(int i = 0 ; i < countOpr; i++){
                    Pattern findId = Pattern.compile("@id"+nesting);

                    int iter = i+1;
                    for(int j=0; j < temp.size();j++) {


                        if (Pattern.matches(".*@id"+nesting+".*", temp.get(j))){
                            Matcher findIdMatcher = findId.matcher(temp.get(j));
                            String newString = "";
                            int pos=0;
                            while(findIdMatcher.find()){
                                newString += temp.get(j).substring(pos,findIdMatcher.start())+String.valueOf(iter);
                                pos = findIdMatcher.end();
                            }
                            newString += temp.get(j).substring(pos,temp.get(j).length());
                            engineInterpreter(newString, nesting + 1);
                        }
                        else{
                            engineInterpreter(temp.get(j), nesting + 1);
                        }

                    }
                }

                interpretLine = "";
                AccumulatorFor.clear();
                }
                else{
                    countEndFor--;
                    AccumulatorFor.add(interpretLine);
                }
            } else {
                AccumulatorFor.add(interpretLine);
            }
```


Данная реализация поддерживает вложенность цикла в цикл. Обработка происходит по следующему принципу:  
1. Первый цикл (в который вложен второй) начинает на каждой итерации генерировать код тела цикла без подстановки и выполнения операторов. Весь результат накаливается в AccumulatorFor, после чего происходит рекурсивный вызов интерпретатора с параметрами: engineInterpreter(AccumulatorFor, nesting + 1);  nesting + 1 -  уровень вложенности цикла, необходимый для конструирования @id(nesting).

#4 Пример работы
##4.1 Генерация Html-шаблона
Программа html.templ  

```
	<style type="text/css">
     .foo1{
		background: #ff392d;
		width: 100%;
     }
     .foo2{
		background: #00ff0b;
		width: 100%;
     }
     .foo3{
		background: #ff00d4;
		width: 100%;
     }
     .foo4{
		background: #0005ff;
		width: 100%;
     }
</style>
{text:["Bag","Hi Bob","Hi Kitty","Hi Morgan","Sun",2,3,3]}
{for=4}
        <div class="foo@id0">{text[@id0]}</div>
{EndFor}
{for=4}
    {ident:@id0}
    {if(ident):[1,3]}
        <div class="foo@id0">{text[@id0]}</div>
    {EndIf}
{EndFor}
```

Сгенерированный код:

```
	<style type="text/css">
     .foo1{
		background: #ff392d;
		width: 100%;
     }
     .foo2{
		background: #00ff0b;
		width: 100%;
     }
     .foo3{
		background: #ff00d4;
		width: 100%;
     }
     .foo4{
		background: #0005ff;
		width: 100%;
     }
</style>
        <div class="foo1">Hi Bob</div>
        <div class="foo2">Hi Kitty</div>
        <div class="foo3">Hi Morgan</div>
        <div class="foo4">Sun</div>
        <div class="foo1">Hi Bob</div>
        <div class="foo3">Hi Morgan</div>
```

Визуальное представление:
![image](https://bitbucket.org/ingvard_/my-exercises/src/4331230c2c4e43bcb25f0996ecfe9d18ab42f262/TemplateRegEx/html_list.png)
Рис 1.

##4.2 Абстрактная программа
Программа:
  
```
-- New variable --
{foo:"Maxim"}
{age:21}
{bar:["first",1,3,5,0]}
    -- Using print --
{foo}
{age}
{bar[0]}
{bar[1]}
    -- Using userTag --
{userTag}
     -- Crazy construction for and if --
{identificator:2}
{for=6}
    {for=3}
        {test:@id0}
        {if(test):[5,6,7,3]}
            pip... @id0
        {EndIf}
        {if(identificator):[2,6,7,3]}
             {foo} First circle iteration: @id0; Second circle iteration: @id1;
        {EndIf}
    {EndFor}
{EndFor}

```

Результат

```
-- New variable --
    -- Using print --
Maxim
21
first
1
    -- Using userTag --
Hi kitty
     -- Crazy construction for and if --
             Maxim First circle iteration: 1; Second circle iteration: 1;
             Maxim First circle iteration: 1; Second circle iteration: 2;
             Maxim First circle iteration: 1; Second circle iteration: 3;
             Maxim First circle iteration: 2; Second circle iteration: 1;
             Maxim First circle iteration: 2; Second circle iteration: 2;
             Maxim First circle iteration: 2; Second circle iteration: 3;
            pip... 3
             Maxim First circle iteration: 3; Second circle iteration: 1;
            pip... 3
             Maxim First circle iteration: 3; Second circle iteration: 2;
            pip... 3
             Maxim First circle iteration: 3; Second circle iteration: 3;
             Maxim First circle iteration: 4; Second circle iteration: 1;
             Maxim First circle iteration: 4; Second circle iteration: 2;
             Maxim First circle iteration: 4; Second circle iteration: 3;
            pip... 5
             Maxim First circle iteration: 5; Second circle iteration: 1;
            pip... 5
             Maxim First circle iteration: 5; Second circle iteration: 2;
           pip... 5
             Maxim First circle iteration: 5; Second circle iteration: 3;
           pip... 6
             Maxim First circle iteration: 6; Second circle iteration: 1;
           pip... 6
             Maxim First circle iteration: 6; Second circle iteration: 2;
            pip... 6
             Maxim First circle iteration: 6; Second circle iteration: 3;
```

#Вывод
В ходе работы был придуман шаблонизатор и реализован интерпретатор, позволяющий упростить процесс разработки веб-разметки.  
Целью работы являлась реализация не какого-то заранее известного подхода, а, прежде всего, своей творческой идеи. 
