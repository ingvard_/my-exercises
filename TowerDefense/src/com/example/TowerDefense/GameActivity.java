package com.example.TowerDefense;

import android.content.SharedPreferences;
import android.graphics.Bitmap;

import java.util.BitSet;

/**
 * Created with IntelliJ IDEA.
 * User: Nasta
 * Date: 21.04.13
 * Time: 23:45
 * To change this template use File | Settings | File Templates.
 */
public class GameActivity extends Thread {
    private GameView gameView;
    private boolean runningFlag;
    public boolean pauseFlag;
    private long lastTime;
    private Generator generator;
    public int speedGame;


    GameActivity(GameView gameView) {
        this.gameView = gameView;
        this.runningFlag = true;
        this.pauseFlag = false;
        speedGame = 1000;

    }

    public void reFlag() {
      if(gameView.life > 0) {
        if (pauseFlag) pauseFlag = false;
        else pauseFlag = true;
      }
    }

    public void run() {
        lastTime = System.currentTimeMillis();
        generator = new Generator(gameView.display.getWidth(), gameView.display.getHeight());
        while (runningFlag) {
            synchronized (gameView.monitor) {
                gameView.monitor.notify();
                if (!pauseFlag) {
                    switch (gameView.mode) {
                        case 0:{
                            if ((System.currentTimeMillis() - lastTime) / speedGame > 1) {
                                generator.gen();
                                int newstat = (int) (Math.random()*100);
                                if(newstat >= 0 && newstat <= 77 )
                                    gameView.items.add(new Coin(generator.x,gameView.display.getHeight()*0.883417, generator.speed, generator.angle, 0.1, generator.direction,"coin",gameView.scoinnew));
                                if(newstat > 77 && newstat <= 96 )
                                    gameView.items.add(new Coin(generator.x,gameView.display.getHeight()*0.883417, generator.speed, generator.angle, 0.1, generator.direction,"snoy",gameView.snoy));
                                if(newstat > 96 && newstat <= 100 )
                                    gameView.items.add(new Coin(generator.x,gameView.display.getHeight()*0.883417, generator.speed, generator.angle, 0.1, generator.direction,"star",gameView.star));
                                lastTime = System.currentTimeMillis();
                                if (speedGame > 200)
                                    speedGame -= 20;
                            }
                            for (int i = 0; i < gameView.items.size(); i++) {
                                gameView.items.get(i).upDate();
                                if (gameView.items.get(i).youIll()) {
                                    gameView.items.remove(i);
                                    i--;
                                    gameView.life--;
                                }
                            }
                        break; }
                        case 2: {
                            if ((System.currentTimeMillis() - lastTime) / 150 > 1) {
                                generator.gen();
                                int newstat = (int) (Math.random()*100);
                                if(newstat >= 0 && newstat <= 50 )
                                    gameView.items.add(new Coin(generator.x,gameView.display.getHeight()*0.883417, generator.speed, generator.angle, 0.1, generator.direction,"cristB",gameView.cristB));
                                if(newstat > 50 && newstat <= 100 )
                                    gameView.items.add(new Coin(generator.x,gameView.display.getHeight()*0.883417, generator.speed, generator.angle, 0.1, generator.direction,"cristR",gameView.cristR));
                                lastTime = System.currentTimeMillis();
                            }
                            for (int i = 0; i < gameView.items.size(); i++) {
                                gameView.items.get(i).upDate();
                                if (gameView.items.get(i).youIll()) {
                                    gameView.items.remove(i);
                                    i--;
                                }
                            }
                        break; }
                        case 1:{
                            if ((System.currentTimeMillis() - lastTime) / speedGame > 1) {
                                generator.gen();
                                gameView.items.add(new Coin(generator.x,gameView.display.getHeight()*0.883417, generator.speed, generator.angle, 0.06, generator.direction,"sblue",gameView.scoinblue));
                                lastTime = System.currentTimeMillis();
                            }
                            for (int i = 0; i < gameView.items.size(); i++) {
                                gameView.items.get(i).upDate();
                                if (gameView.items.get(i).youIll()) {
                                    gameView.items.remove(i);
                                    i--;
                                    gameView.life--;
                                }
                            }
                        break;
                        }



                    }
                    if (gameView.life <= 0) {
                        pauseFlag = true;
                        SharedPreferences.Editor editor = gameView.record.edit();
                        editor.putInt("record", gameView.count);
                        editor.commit();
                    }


                    try {
                        gameView.monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        }
    }
}
