package com.example.TowerDefense;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

public class GameScene extends Activity {
    public Display display;
    private GameView gameView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        display = getWindowManager().getDefaultDisplay();
        gameView = new GameView(this);
        setContentView(gameView); //Назначаем класс GameView за отображение данного активити
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {      //Метод оброботки поворота экрана
        super.onConfigurationChanged(newConfig);
    }
    @Override
    public void onPause() {
        super.onPause();
        gameView.gameActivity.reFlag();
    }
}
