package com.example.TowerDefense;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

/**
 * Created with IntelliJ IDEA.
 * User: Nasta
 * Date: 24.04.13
 * Time: 0:40
 * To change this template use File | Settings | File Templates.
 */
public class ButtonScene {
    public int x;
    public int y;
    public int sizeFrameX;
    public int sizeFrameY;
    private int state;
    private int speed;
    private int countFrame;
    private Ret pic;

    ButtonScene(Bitmap bmp, int x, int y, int sizeFrameX, int sizeFrameY, int countFrame, int speed) {
        this.pic = new Ret(bmp);
        this.x = x;
        this.y = y;
        this.sizeFrameX = bmp.getWidth()/countFrame;
        this.sizeFrameY = bmp.getHeight();
        this.countFrame = countFrame;
        this.speed = speed;
        this.state = 0;
    }

    public void setCoordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void update() {
        if (state == countFrame - 1) {
            state = 0;
        } else state++;
    }

    public Ret getPicture() {
        pic.set(x, y, sizeFrameX, sizeFrameY, state);
        return pic;
    }

    public void drawPicture(Canvas canvas) {
        pic.set(x, y, sizeFrameX, sizeFrameY, state);
        canvas.drawBitmap(pic.bmp, pic.src, pic.dst, null);
    }



    class Ret {
        public Rect src;
        public Rect dst;
        public Bitmap bmp;

        Ret(Bitmap bmp) {
            this.bmp = bmp;
        }

        public void set(int x, int y, int width, int height, int frame) {
            src = new Rect(width * frame, 0, width * frame + width, height);
            dst = new Rect(x, y, x + width, y + height);
        }
    }


}
