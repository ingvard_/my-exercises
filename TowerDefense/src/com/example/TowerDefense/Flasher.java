package com.example.TowerDefense;

/**
 * Created with IntelliJ IDEA.
 * User: ingvard
 * Date: 21.05.13
 * Time: 11:19
 * To change this template use File | Settings | File Templates.
 */
public class Flasher {
    private long timeLife;
    private int clk;
    private long timeClk;
    private long timeStart;
    private boolean flag;
    Flasher(long timeLife,int clk){
        this.timeLife = timeLife;
        this.clk = clk;
        this.timeClk = System.currentTimeMillis();
        this.timeStart = this.timeClk;
        this.flag = true;
    }
    public int getStatus(){
        if(timeLife+timeStart < System.currentTimeMillis())
            return -1;
        if(timeClk+clk < System.currentTimeMillis()){
            flag = !flag;
            timeClk = System.currentTimeMillis();
        }

        if(flag)
            return 1;
        else
            return 0;
    }


}
