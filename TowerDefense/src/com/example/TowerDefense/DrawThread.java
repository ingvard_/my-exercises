package com.example.TowerDefense;


import android.graphics.Canvas;

/**
 * Created with IntelliJ IDEA.
 * User: Nasta
 * Date: 21.04.13
 * Time: 1:21
 * To change this template use File | Settings | File Templates.
 */
public class DrawThread extends Thread {
    private GameView view;
    private boolean running = false;

    public DrawThread(GameView view) {
        this.view = view;
    }

    public void setRunning(boolean run) {
        running = run;
    }

    @Override
    public void run() {
        while (running) {
            synchronized (view.monitor) {
                view.monitor.notify();
                Canvas c = null;
                try {
                    c = view.getHolder().lockCanvas();
                    synchronized (view.getHolder()) {
                        view.onDraw(c);
                    }
                } finally {
                    if (c != null) {
                        view.getHolder().unlockCanvasAndPost(c);
                    }
                }
                try {
                    view.monitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }

        }
    }

}
