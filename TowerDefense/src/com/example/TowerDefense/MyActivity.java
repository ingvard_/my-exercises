package com.example.TowerDefense;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;

public class MyActivity extends Activity {
    public boolean hold;
    public Display display; // Сохраняем параметры дисплея

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); // Убираем 1 верхнюю полоску
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //Убираем 2 верхнюю полоску
        setContentView(R.layout.main);
        display = getWindowManager().getDefaultDisplay(); //Получаем параметры дисплея
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) { //Перегружаем метод отвечающий за поворот дисплея
        super.onConfigurationChanged(newConfig);
    }

    public boolean onTouchEvent(MotionEvent event) {    //Обработка прикосновения к дисплею
        int yT;
        int xT;
        if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
            hold = false;
        } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
            hold = true;
            xT = (int) event.getX();
            yT = (int) event.getY();

            if (display.getWidth() * 0.1125 < xT && display.getWidth() * 0.58125 > xT &&         //Проверяем попали ли мы в область кнопки new game
                    display.getHeight() * 0.21875 < yT && display.getHeight() * 0.3875 > yT) {
                Intent intent = new Intent(this, GameScene.class); //Создаём 2 активити
                startActivity(intent); //Переходим во 2 активити
            }

        }

        return true;
    }
}
