package com.example.TowerDefense;

/**
 * Created with IntelliJ IDEA.
 * User: Nasta
 * Date: 22.04.13
 * Time: 0:54
 * To change this template use File | Settings | File Templates.
 */
public class Generator {

    public int x;
    public int speed;
    public int angle;
    public int direction;
    private int borderWidth;
    private int borderHeight;

    Generator(int width, int height) {
        borderWidth = width;
        borderHeight = height;
    }

    public void gen() {
        x = 20 + (int) (Math.random() * (borderWidth - 40));
        angle = 30 + (int) (Math.random() * 55);
        direction = 1;
        double propor = 480/borderHeight;
        int status = (int) (Math.random() * 3);


        switch (status) {
            case 0: {
                int addSpeed = (int) (Math.random() * 16);
                int addX = (int) (Math.random() * 30);
                speed = 60 + addSpeed +(int)(24-propor*24);
                angle = 85;
                x = 20 + addX;
                break;
            }
            case 1: {
                int addSpeed = (int) (Math.random() * (10+(int)(10-propor*10)));
                int addX = (int) (Math.random() * 90);
                speed = 70 + addSpeed  +(int)(14-propor*14);
                angle = 90;
                x = 40 + addX;
                break;
            }
            case 2: {
                direction = -1;
                int addSpeed = (int) (Math.random() * 18);
                int addX = (int) (Math.random() * 40);
                speed = 60 + addSpeed+(int)(24-propor*24);
                angle = 85;
                x = 300 - addX;
                break;
            }
            default: {
                direction = -1;
                int addSpeed = (int) (Math.random() * 18);
                int addX = (int) (Math.random() * 40);
                speed = 60 + addSpeed+(int)(24-propor*24);
                angle = 85;
                x = 300 - addX;
                break;
            }
        }
    }
}
