package com.example.TowerDefense;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.*;
import android.util.DisplayMetrics;
import android.view.*;

import java.util.ArrayList;
import java.util.List;

public class GameView extends SurfaceView {

    public Object monitor = new Object();  //Объект для синхронизация между рисующим и рассчётным потоком
    public List<Coin> items;  // Список монеток находящахся на дисплее
    public int life;  //Число жизний
    boolean hold;
    private DrawThread drawThread;
    public GameActivity gameActivity;
    private SurfaceHolder holder;
    private Bitmap endGame;
    private Bitmap pauseGame;
    private Bitmap platform;
    private long modeTime;
    private long snowTime;
    private int textSize;
    private Bitmap gameBackground;
    private Paint paint;
    public int count;
    private ButtonScene gameState;
    private ButtonScene reGame;
    public int mode;

    public Bitmap cristB;
    public Bitmap cristR;
    public Bitmap star;
    public Bitmap snoy;
    public Bitmap scoinnew;
    public Bitmap scoinblue;
    public Bitmap newrecord;
    public SharedPreferences record;
    private List<EffectSnow> snow;
    private Bitmap snowPic;
    private Flasher flasher;
    Display display;


    public GameView(Context context) {
        super(context);
        record = context.getSharedPreferences("record", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = record.edit();
        if(record.getInt("record",-1) == -1){
            editor.putInt( "record", 0 );
            editor.commit();
        }

        //Init containers
        flasher = new Flasher(10000,1000);
        display = ((GameScene)context).display;//display;
        gameState = new ButtonScene(BitmapFactory.decodeResource(getResources(), R.drawable.playpaus), (int)(((double)13/320)*display.getWidth()),
                (int)(((double)430/480)*display.getHeight()), 50, 50, 2, 0);
        reGame = new ButtonScene(BitmapFactory.decodeResource(getResources(), R.drawable.regame), (int)(((double)270/320)*display.getWidth()),
                (int)(((double)433/480)*display.getHeight()), 45, 45, 1, 0);

        items = new ArrayList<Coin>();
        snow = new ArrayList<EffectSnow>();
        paint = new Paint();
        mode = 0;

        //Init resources
        Resources resources = getResources();
        life = resources.getInteger(R.integer.life);
        count = resources.getInteger(R.integer.startCount);
        textSize = resources.getInteger(R.integer.textSize);
        paint.setColor(Color.WHITE);
        paint.setTextSize(textSize);
        gameBackground = BitmapFactory.decodeResource(getResources(), R.drawable.gamebackground);
        endGame = BitmapFactory.decodeResource(getResources(), R.drawable.gameover);

        /*Игровые картинки*/
        cristB= BitmapFactory.decodeResource(getResources(), R.drawable.cristb);
        cristR= BitmapFactory.decodeResource(getResources(), R.drawable.cristr);
        star= BitmapFactory.decodeResource(getResources(), R.drawable.star);
        snoy= BitmapFactory.decodeResource(getResources(), R.drawable.snoy);
        scoinnew= BitmapFactory.decodeResource(getResources(), R.drawable.scoinnew);
        scoinblue= BitmapFactory.decodeResource(getResources(), R.drawable.scoinblue);
        newrecord= BitmapFactory.decodeResource(getResources(), R.drawable.newrecord);
        snowPic= BitmapFactory.decodeResource(getResources(), R.drawable.snoweff);
        /* ---*/
        pauseGame = BitmapFactory.decodeResource(getResources(), R.drawable.pausetab);
        platform = BitmapFactory.decodeResource(getResources(), R.drawable.platform);
        //Creat two thread
        drawThread = new DrawThread(this);
        gameActivity = new GameActivity(this);

        holder = getHolder();
        gameActivity.start();
        holder.addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                boolean retry = true;
                drawThread.setRunning(false);
                while (retry) {
                    try {
                        drawThread.join();
                        retry = false;
                    } catch (InterruptedException e) {
                    }
                }
            }

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                drawThread.setRunning(true);
                drawThread.start();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format,
                                       int width, int height) {
            }
        });

    }

    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(gameBackground,new Rect(0,0,gameBackground.getWidth(),gameBackground.getHeight()),new Rect(0,0,display.getWidth(),display.getHeight()), null);
        canvas.drawText(String.valueOf(count), (float)(gameBackground.getWidth()*0.234375), (float)(display.getHeight()*0.11458), paint);
        canvas.drawText(String.valueOf(life), (float)(gameBackground.getWidth()*0.875), (float)(display.getHeight()*0.10625), paint);
        if(mode == 2){

            canvas.drawBitmap(platform, (float)(0), (float)(display.getHeight()*0.875), null);
            if(modeTime < System.currentTimeMillis()-10000){
                mode = 0;

                        items.clear();


            }
        }
        if(record.getInt("record",-1) < count)  {
              if(flasher.getStatus() != -1 &&  flasher.getStatus() != 0)
                  canvas.drawBitmap(newrecord, 100, 100, null);
        }

        if(mode == 1){
            if(modeTime < System.currentTimeMillis()-10000)
            {
                snow.clear();
                mode = 0;
            }
            if(System.currentTimeMillis() - 500 > snowTime){
                snowTime = System.currentTimeMillis();
                snow.add(new EffectSnow());
            }
            for(int i=0; i < snow.size();i++) {
                canvas.drawBitmap(snowPic,snow.get(i).x+snow.get(i).shift,snow.get(i).y, null);
                snow.get(i).update();

            }

        }

        for (int i = 0; i < items.size(); i++)
            items.get(i).drawCoin(canvas,!gameActivity.pauseFlag);//canvas.drawBitmap(bmp, (int) items.get(i).x, (int) items.get(i).y, null);
        gameState.drawPicture(canvas);
        reGame.drawPicture(canvas);
        if (life == 0) canvas.drawBitmap(endGame, 0, 0, null);
        if (gameActivity.pauseFlag && life != 0) canvas.drawBitmap(pauseGame, (float)(((double)75/320)*gameBackground.getWidth()), (float)(((double)180/480)*gameBackground.getHeight()), null);

    }

    public boolean onTouchEvent(MotionEvent event) {
        int yT;
        int xT;
        if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
            hold = false;
        } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
            hold = true;
            xT = (int) event.getX();
            yT = (int) event.getY();
            // Pause and play
            if (gameState.x < xT && xT < gameState.x + gameState.sizeFrameX && gameState.y < yT && yT < gameState.y + gameState.sizeFrameY && life != 0) {
                gameActivity.reFlag();
                gameState.update();
            }
            if (reGame.x < xT && xT < reGame.x + reGame.sizeFrameX && reGame.y < yT && yT < reGame.y + reGame.sizeFrameY) {
                life = 3;
                count = 0;
                gameActivity.speedGame = 1000;
                gameActivity.pauseFlag = false;
                for (int i = 0; i < items.size(); i++) {

                    items.remove(i);
                }
            }
            // Click on coin
            for (int i = 0; i < items.size(); i++) {
                if ((int) items.get(i).x < xT+5 && (int) items.get(i).x > xT - scoinnew.getHeight() &&
                        (int) items.get(i).y < yT+5 && (int) items.get(i).y > yT - scoinnew.getHeight() && !gameActivity.pauseFlag) {
                    if(items.get(i).name == "star"){
                        mode = 2;
                        modeTime = System.currentTimeMillis();
                    }
                    if(items.get(i).name == "snoy"){
                        mode = 1;
                        modeTime = System.currentTimeMillis();
                    }
                    if(items.get(i).name == "cristR"){
                        life++;

                    }
                    if(items.get(i).name == "cristB"){
                        count+=10;
                    }

                    items.remove(i);
                    i--;
                    count++;
                }
            }
        }
        return true;
    }


}
