package com.example.TowerDefense;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

/**
 * Created with IntelliJ IDEA.
 * User: Nasta
 * Date: 21.04.13
 * Time: 20:21
 * To change this template use File | Settings | File Templates.
 */
public class Coin {
    /* Coin varebles*/
    public double x;
    public double y;
    private double hight;
    private double speed;
    private double angle;
    private long createTime;
    private int g;
    private double step;
    private double t;
    private double xStart;
    private int direction;
    private Bitmap bmp;
    private int sprite;
    private long lastTime;
    public String name;

    Coin(double x, double y, double speed, double angle, double step, int direction,String name, Bitmap bmp) {
        g=9;
        this.xStart = x;
        this.speed = speed;
        this.angle = angle;
        this.createTime = System.currentTimeMillis();
        this.hight = y;
        this.t = 0;
        this.step = step;
        this.direction = direction;
        this.bmp = bmp;
        sprite = 0;
        lastTime = 0;
        this.name = name;
    }

    public boolean youIll() {
        if (x < 0 || x > 320) return true;
        if (y < 0 || y > hight) return true;
        return false;
    }

    public void upDate() {
        t += step;
        x = xStart + direction * ((speed * Math.cos(Math.toRadians(angle))) * t);
        y = hight - (((speed * Math.sin(Math.toRadians(angle))) * t - g * t * t / 2));
    }

    public void drawCoin(Canvas canvas,boolean activ) {
        //canvas.drawBitmap(bmp, (int) items.get(i).x, (int) items.get(i).y, null);
        if((System.currentTimeMillis() - lastTime)/60 > 1 && activ)
        {
            sprite++;
            if (sprite == 8) sprite = 0;
            lastTime = System.currentTimeMillis();
        }
        double x1 = bmp.getWidth()/8;
        double y1 = bmp.getHeight();
        Rect src = new Rect((int)(sprite*x1), 0,(int)(sprite*x1 + x1), (int)y1);
        Rect dst = new Rect((int) x, (int) y, (int) x + (int)x1, (int) y + (int)y1);
        canvas.drawBitmap(bmp, src, dst, null);
    }


}
