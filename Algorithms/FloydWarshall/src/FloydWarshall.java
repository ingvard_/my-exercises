import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class FloydWarshall {
    public static void writeWay(int x, int y, int[][] p) {
        if (p[x][y] == -1) {
            System.out.println("Sorry,but there is no way!");
        } else {
            System.out.print("{" + p[x][y] + "," + y + "}");
            if (x != p[x][y])
                writeWay(x, p[x][y], p);

        }
    }
    public static void main(String[] arg) throws IOException {

        int inf = 9999999;
        BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\ingvard\\IdeaProjects\\FloydWarshall\\src\\input.txt"));
        String line = reader.readLine();
        int n = Integer.valueOf(line) + 1;

        int[][] matrix = new int[Integer.valueOf(line) + 1][Integer.valueOf(line) + 1];
        int[][] p = new int[Integer.valueOf(line) + 1][Integer.valueOf(line) + 1];

        for (int i = 0; i < n; i++) {
            Arrays.fill(matrix[i], inf);
            Arrays.fill(p[i], -1);
        }
        System.out.println("Adjacency Matrix Representation:");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++)
                System.out.print((matrix[i][j] == 9999999 ? "INF" : matrix[i][j] + " ") + " ");
            System.out.println();
        }

        while ((line = reader.readLine()) != null) {
            String[] token = line.split(" ");
            matrix[Integer.valueOf(token[0])][Integer.valueOf(token[1])] = Integer.valueOf(token[2]);
            p[Integer.valueOf(token[0])][Integer.valueOf(token[1])] = Integer.valueOf(token[0]);
        }

        for (int k = 0; k < n; ++k) {
            for (int i = 0; i < n; ++i) {
                for (int j = 0; j < n; ++j) {
                    if (matrix[i][k] < inf && matrix[k][j] < inf) {
                        matrix[i][j] = Math.min(matrix[i][j], matrix[i][k] + matrix[k][j]);
                        if (matrix[i][j] > matrix[i][k] + matrix[k][j]) {
                            p[i][j] = p[i][j];
                        } else {
                            p[i][j] = p[k][j];
                        }
                    }
                }
            }
        }
        System.out.println("Adjacency Matrix all dist:");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++)
                System.out.print((matrix[i][j] == 9999999 ? "INF" : matrix[i][j] + " ") + " ");
            System.out.println();
        }
        System.out.println("Adjacency Matrix path:");
        System.out.println();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++)
                System.out.print(p[i][j] + " ");
            System.out.println();
        }
        System.out.println("Path: v2 --> v7 dist = " + matrix[2][7]);
        writeWay(2, 7, p);

    }
}


/*
 8
1 3 3
3 5 22
5 7 19
1 4 2
3 6 12
6 7 9
1 6 21
4 6 11
6 8 6
1 8 8
4 8 30
7 8 10
2 4 17
5 6 1


7
1 0 6
5 1 4
1 6 1
6 4 4
4 2 6
*/