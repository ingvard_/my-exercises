import java.io.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ingvard
 * Date: 21.03.13
 * Time: 23:51
 * To change this template use File | Settings | File Templates.
 */
public class BreadthFirstSearch {


    public static int [] u;
    public static int [] d;
    public static int [] p;
    public static void bFS(List<Integer>[] graph,int vertex)
    {
        Queue<Integer> qe=new LinkedList<Integer>();
        int targ;
        d[vertex]=0;
        u[vertex]=1;
        qe.add(vertex);
        while(qe.size() != 0)
        {
           targ=qe.remove();
           //d[targ]=d[p[targ]]+1;
           //u[targ]=1;
           for(int i=0; i < graph[targ].size();i++)
           {
               if(u[graph[targ].get(i)] == 0)
               {
                   qe.add(graph[targ].get(i));
                   u[graph[targ].get(i)]=1;
                   d[graph[targ].get(i)]=d[targ]+1;
                   p[graph[targ].get(i)]=targ;
               }
           }

        }

    }
    public static void main(String[] arg) throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\ingvard\\IdeaProjects\\BreadthFirstSearch\\src\\input.txt"));
        String line=reader.readLine();
        String[] token = line.split(" ");
        int n=Integer.valueOf(token[0]);
        @SuppressWarnings("unchecked")
        List<Integer>[] lists = new List[n+1];
        u=new int[n+1];
        d=new int[n+1];
        p=new int[n+1];
        Arrays.fill(d,-1);
        Arrays.fill(u,0);
        Arrays.fill(p,0);
        for (int i = 0; i <= n; i++)
            lists[i] = new ArrayList<Integer>();
        while ((line=reader.readLine()) != null)
        {
            token=line.split(" ");
            lists[Integer.valueOf(token[0])].add(Integer.valueOf(token[1]));
        }
        bFS(lists,1);
        BufferedWriter output = new BufferedWriter(new FileWriter("C:\\Users\\ingvard\\IdeaProjects\\BreadthFirstSearch\\src\\output.txt"));
        for(int i=1;i<lists.length;i++)
            output.write(d[i]+" ");
        output.close();

    }
}
