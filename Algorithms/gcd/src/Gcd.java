/**
 * Created with IntelliJ IDEA.
 * User: ingvard
 * Date: 23.02.13
 * Time: 13:51
 * To change this template use File | Settings | File Templates.
 */
public class Gcd {
    public int foo( int n, int a, int b)
    {
        if (n <= 0)
            return 1;
        else
            return foo(n - 1, a, b) +
                    a * foo(n / 2, a, b) + b;
    }
    public static void main(String[] args){
        System.out.println(Gcd.foo(7,1,1));
    }
}
