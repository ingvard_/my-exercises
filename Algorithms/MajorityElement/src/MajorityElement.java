import java.io.*;
import java.util.*;
/**
 * Created with IntelliJ IDEA.
 * User: ingvard
 * Date: 17.03.13
 * Time: 15:59
 * To change this template use File | Settings | File Templates.
 */
public class MajorityElement {
    public static void main(String[] arg) throws IOException{
        BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\ingvard\\IdeaProjects\\MajorityElement\\src\\input.txt"));
        int k,n;
        String line = reader.readLine();
        String[] temp = line.split(" ");
        k = Integer.valueOf(temp[0]);
        n = Integer.valueOf(temp[1]);
        int[][] arr = new int[k][n];
        for(int i=0;(line = reader.readLine()) != null;i++)
        {
           temp=line.split(" ");
            int j=0;
            for(String str : temp){
              arr[i][j]=Integer.valueOf(str);
              j++;
            }
            Arrays.sort(arr[i]);
           /* for(int z=0; z < n; z++)
                System.out.print(arr[i][z]+" ");
            System.out.println(); */
        }
        for(int i=0; i < k ;i++ ){
            int counter=0;
            int counterT=0;
            int targetItem=0;
            int targetNext=0;
            for(int j=1; j < n;j++){
                if(arr[i][j] != arr[i][targetItem]){
                     if(arr[i][targetNext] != arr[i][j]){
                           if(counter < counterT)
                           {
                               targetItem=targetNext;
                               counter=counterT;
                               targetNext=j;
                               counterT=1;
                           }
                         else
                         {
                             targetNext=j;
                             counterT=1;
                         }
                     }
                    else counterT++;
                }
                else counter++;
            }
            if(n/2 <= counter) System.out.print(arr[i][targetItem]+" ");
            else System.out.print(-1+" ");
           // System.out.println(counter);
        }


    }
}
