/**
 * Created with IntelliJ IDEA.
 * User: ingvard
 * Date: 19.03.13
 * Time: 22:17
 * To change this template use File | Settings | File Templates.
 */
import java.io.*;
import java.util.*;


public class DoubleDegreeArray {
    public static int bfs(List<Integer>[] arr,int n,int i) {
        int result = -1;
        int[] d = new int[n+1];
        int[] u = new int[n+1];
        for(int j=0; j <= n;j++)u[j]=d[j]=0;
        Queue<Integer> qu = new LinkedList<Integer>();
        qu.add(i);
        int t;
        d[0]=-1;
        while(qu.size() != 0){
            t=qu.remove();
            d[t]=d[u[t]]+1;
            if(t == 1)
            {
                result=d[t];
                break;
            }
            for(int j=0;j < arr[t].size();j++)
            {
                if(d[arr[t].get(j)] == 0){
                    qu.add(arr[t].get(j));
                    u[arr[t].get(j)]=t;
                }

            }
        }
        return result;
    }
    public static void main(String[] arg) throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\ingvard\\IdeaProjects\\DoubleDegreeArray\\src\\input.txt"));
        String line = reader.readLine();
        String[] token = line.split(" ");
        int n=Integer.valueOf(token[0]);
        @SuppressWarnings("unchecked")
        List<Integer>[] lists = new List[n+1];
        for (int i = 0; i <= n; i++)
            lists[i] = new ArrayList<Integer>();

        while((line=reader.readLine()) != null)
        {
            token=line.split(" ");
            //lists[Integer.valueOf(token[0])].add(Integer.valueOf(token[1]));
            lists[Integer.valueOf(token[1])].add(Integer.valueOf(token[0]));
        }
        BufferedWriter output = new BufferedWriter(new FileWriter("C:\\Users\\ingvard\\IdeaProjects\\DoubleDegreeArray\\src\\output.txt"));
        //output.write(0+" ");
        for(int i=1; i <= n;i++)
        {
           // System.out.print(i + ": ");

            output.write(bfs(lists,n,i)+" ");
            //System.out.println();
        }
       //System.out.print(bfs(lists,n,3));
        output.close();

    }
}
