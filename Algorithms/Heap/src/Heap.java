/**
 * Created with IntelliJ IDEA.
 * User: ingvard
 * Date: 18.03.13
 * Time: 16:54
 * To change this template use File | Settings | File Templates.
 */
import java.io.*;
public class Heap {
    public static void shiftUp(int[] a,int i)
    {
        int temp;
        if(i != 0)
        {
            if(a[i] > a[i/2])
            {
                temp=a[i/2];
                a[i/2]=a[i];
                a[i]=temp;
                shiftUp(a,i/2);
            }
        }

    }
    public static void shiftDown(int[] a,int i,int count)
    {
            if(i != 0 && (2*i+1) < a.length)
            {
                if(a[i] < a[2*i] && a[i] < a[2*i+1]){
                    int temp;
                    if(a[2*i] > a[2*i+1])
                    {
                        temp=a[i];
                        a[i]=a[2*i];
                        a[2*i]=temp;
                        shiftDown(a,i*2,count);
                    }
                    else
                    {
                        temp=a[i];
                        a[i]=a[2*i+1];
                        a[2*i+1]=temp;
                        shiftDown(a,2*i+1,count);
                    }
                }
            }
            else
            {
                if(a[0] < a[2] && a[0] < a[3]){
                    int temp;
                    if(a[2] > a[3])
                    {
                        temp=a[0];
                        a[0]=a[2];
                        a[2]=temp;
                        shiftDown(a,2,count);
                    }
                    else
                    {
                        temp=a[0];
                        a[0]=a[3];
                        a[3]=temp;
                        shiftDown(a,3,count);
                    }
                }
            }

    }
    public static int extractMax(int[] a,int count)
    {
        int result=a[0];
        if(count > 0 && count <= a.length)
        {
            a[0]=a[count-1];
            shiftDown(a,0,count-2);
            count--;
        }
        return result;
    }
    public static void main(String[] arg) throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\ingvard\\IdeaProjects\\BuildingHeap\\src\\input.txt"));
        String line = reader.readLine();
        int n=Integer.valueOf(line);
        int[] arr = new int[n];
        int countInHeap=0;
        line = reader.readLine();
        String[] token = line.split(" ");
        int i=0;
        for(String str : token)
        {
            arr[i]=Integer.valueOf(str);
            //shiftUp(arr,i);
            countInHeap++;
            i++;
        }

        //arr[0]=1;
        //shiftDown(arr,0,countInHeap);
        //7 5 1 3 2
        //1 3 5 7 2 ip
        BufferedWriter output = new BufferedWriter(new FileWriter("C:\\Users\\ingvard\\IdeaProjects\\BuildingHeap\\src\\output.txt"));
        for(i=0;i<n;i++)
            System.out.print(arr[i]+" ");
        output.close();
    }
}
