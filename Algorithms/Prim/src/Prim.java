import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

class Pair {
    public int x;
    public int w;

    Pair(int x, int w) {
        this.x = x;
        this.w = w;
    }
}

public class Prim {
    public static void main(String[] arg) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\ingvard\\IdeaProjects\\Prim\\src\\input.txt"));
        String line = reader.readLine();
        int n = Integer.valueOf(line);
        List<Pair>[] graph = new List[n + 1];
        int[] u = new int[n + 1];
        int[] cost = new int[n + 1];
        int[] p = new int[n + 1];
        Arrays.fill(u, 0);
        Arrays.fill(p, -1);
        Arrays.fill(cost, 9999999);
        for (int i = 0; i <= n; i++) {
            graph[i] = new ArrayList<Pair>();
        }
        while ((line = reader.readLine()) != null) {
            String[] token = line.split(" ");
            graph[Integer.valueOf(token[0])].add(new Pair(Integer.valueOf(token[1]), Integer.valueOf(token[2])));
            graph[Integer.valueOf(token[1])].add(new Pair(Integer.valueOf(token[0]), Integer.valueOf(token[2])));
        }
        PriorityQueue<Integer> a;


        PriorityQueue<Pair> queue = new PriorityQueue<Pair>(10, new Comparator<Pair>() {
            public int compare(Pair x, Pair y) {
                return x.w > y.w ? 1 : (x.w < y.w ? -1 : 0);
            }
        });

        int s = 2;
        queue.add(new Pair(s, 0));
        u[s] = 1;
        cost[s] = 0;

        while (!queue.isEmpty()) {
            Pair t = queue.remove();
            u[t.x] = 1;
            for (int i = 0; i < graph[t.x].size(); i++) {
                if (u[graph[t.x].get(i).x] == 0) {
                    queue.add(graph[t.x].get(i));
                    if (cost[graph[t.x].get(i).x] > graph[t.x].get(i).w) {
                        p[graph[t.x].get(i).x] = t.x;
                        cost[graph[t.x].get(i).x] = graph[t.x].get(i).w;
                    }
                }
            }

        }
        for (int i = 1; i <= n; i++)
            System.out.println(i + " " + p[i]);


    }
}
