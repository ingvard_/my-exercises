import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: ingvard
 * Date: 17.03.13
 * Time: 17:21
 * To change this template use File | Settings | File Templates.
 */
public class MergeTwoSortedArrays {
    static int[] merge(int[] a,int[] b)
    {
        int allSize=a.length+b.length;
        int[] c=new int[allSize];
        for(int i=a.length,j=b.length,k=0;k < allSize;k++){
           //Сливаем до конца 1 из массивов
            while(i != 0 && j != 0)
            {
                  if(a[a.length-i] > b[b.length-j])
                  {
                       c[k]=b[b.length-j];
                       j--;
                       k++;
                  }
                  else
                  {
                       c[k]=a[a.length-i];
                       i--;
                       k++;
                  }
            }
            if(j == 0){
                for(;i != 0 ;i--)
                {
                    c[k]=a[a.length-i];
                }
            }
            if(i == 0){
                for(;j != 0 ;j--)
                {
                    c[k]=b[b.length-j];
                }
            }

        }
        return c;
    }
    public static void main(String[] arg) throws IOException{
        int n;
        BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\ingvard\\IdeaProjects\\MergeTwoSortedArrays\\src\\input.txt"));
        String line;
        line=reader.readLine();
        int[] a=new int[Integer.valueOf(line)];
        line=reader.readLine();
        String[] token = line.split(" ");
        n=0;
        for(String str : token)
        {
            a[n]=Integer.valueOf(str);
            n++;
        }
        line=reader.readLine();
        int[] b=new int[Integer.valueOf(line)];
        line=reader.readLine();
        token = line.split(" ");
        n=0;
        for(String str : token)
        {
            b[n]=Integer.valueOf(str);
            n++;
        }
        int[] c=merge(a,b);
        BufferedWriter out = new BufferedWriter(new FileWriter("C:\\Users\\ingvard\\IdeaProjects\\MergeTwoSortedArrays\\src\\output.txt"));
        for(int i=0;i < c.length;i++)
            out.write(c[i]+" ");
            out.close();

    }
}
