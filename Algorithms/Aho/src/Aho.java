import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;


class Node {
    public Node[] son;    // Список сыновей
    public Node go;
    public Node suffixLink;
    public Node parent;  // ссылка на родителя
    public boolean end;  // терменатор
    public int num; //Нумерация вершины
    public char charToParent; // Буква в вершине


    Node(int countSon) {
        son = new Node[countSon];
    }
}

class Tree {
    public int countNode;
    private Node root;
    private int powerAlp;

    Tree() {
        this.powerAlp = 80;
        this.countNode = 1;
        this.root = new Node(this.powerAlp);
        this.root.num = 0;
    }

    public void buildGo(Node node) {
        if (node.suffixLink != null && node != root) {
            if (node.parent.end == true) {
                node.go = node.parent;
            } else {
                if (node.parent.go != null){
                    node.go = node.parent.go;
                }
                else
                {
                    node.go = node.parent;
                }
            }
        }

    }

    public void goLink(Node node) {

            for (Node son : node.son) {
                if (son != null) {
                    buildGo(son);
                    goLink(son);
                }
            }

    }

    public Node searchLink(Node u, char a) {

        int index = a - 'A';
        if (u == root)
            return root;
        if (u.suffixLink.son[index] != null) {
            return u.suffixLink.son[index];
        } else {
            return searchLink(u.parent, a);
        }

    }

    public void buildSuffixLink() {
        Queue<Node> nodeQueue = new LinkedList<Node>();
        // Создали ссылки на корень для 1 уровня вершин, и добавили их в очередь
        for (int i = 0; i < this.powerAlp; i++) {
            if (root.son[i] != null) {
                root.son[i].suffixLink = root;
                nodeQueue.add(root.son[i]);
            }
        }
        //Производим обход в ширину
        while (nodeQueue.size() != 0) {
            Node qeNode = nodeQueue.remove();
            qeNode.suffixLink = searchLink(qeNode.parent, qeNode.charToParent);  // Рекурсивно вычисляем суффиксные ссылки
            for (Node son : qeNode.son) {
                if (son != null) {
                    nodeQueue.add(son);
                }
            }
        }
        goLink(root);


    }

    public void addString(String str) {
        Node cur = root;
        for (int i = 0; i < str.length(); i++) {
            int index = str.charAt(i) - 'A';
            if (cur.son[index] == null) {
                cur.son[index] = new Node(this.powerAlp);
                cur.son[index].parent = cur;
                cur.son[index].end = false;
                cur.son[index].go = null;
                cur.son[index].suffixLink = null;
                cur.son[index].num = (countNode++);
                cur.son[index].charToParent = str.charAt(i);
            }
            cur = cur.son[index];
        }
        cur.end = true;
        // aT(root);
    }

    public void iPM(Node g) //Introduction to Pattern Matching
    {
        if (g != root) System.out.println((g.parent.num + 1) + " " + (g.num + 1) + " " + g.charToParent);
        for (int i = 0; g.son.length > i; i++) {
            if (g.son[i] != null) {
                iPM(g.son[i]);
            }
        }

    }

    public void aT(Node g) //The Augmented Trie
    {
        if (g != root) System.out.println((g.parent.num + 1) + " " + (g.num + 1) + " " + g.charToParent);
        for (Node son : g.son) {
            if (son != null) {
                iPM(son);
            }
        }
        if (g.suffixLink != null) {
            System.out.println((g.num + 1) + " " + (g.suffixLink.num + 1) + " S");
        }

    }
}

public class Aho {
    public static void main(String[] arg) throws IOException {
        Tree myTree = new Tree();


        BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\ingvard\\IdeaProjects\\Aho\\src\\input.txt"));
        String text = reader.readLine();
        String line;
        while ((line = reader.readLine()) != null) {
            myTree.addString(line);
        }
        myTree.buildSuffixLink();
        //  System.out.println(myTree.countNode);
    }
}
