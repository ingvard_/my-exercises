import com.xeiam.xchart.BitmapEncoder;
import com.xeiam.xchart.Chart;
import com.xeiam.xchart.SwingWrapper;

import java.io.IOException;

class Pair {
    public double x;
    public double y;

    Pair(double x, double y) {
        this.x = x;
        this.y = y;
    }
}

public class MathModel {

    public static void block1(double x2, double x1, double x4, double p2) {
        double x3 = 1.2 * x4 + x1 - x2 + 1;
        double p3 = (x3 - x1) / ((1 - x3) * Math.exp(x4 / (1 + (x4 / 20))));
        System.out.println("(" + p2 + "," + p3 + ")");
    }

    public static Pair solveBlock2X4(double x2) {
        double x41, x42;
        x41 = (-((1.2 * (0.9 - 0.12 * x2) / 10) - 0.84 - 0.096 * x2) + Math.sqrt((((1.2 * (0.9 - 0.12 * x2) / 10) - 0.84 - 0.096 * x2) * ((1.2 * (0.9 - 0.12 * x2) / 10) - 0.84 - 0.096 * x2)) - 4 * ((0.144 + (1.2 * (0.9 - 0.12 * x2) / 400))) * ((1.2 * (0.9 - 0.12 * x2) + 0.82 * x2 - 0.8 - 0.02 * x2 * x2)))) / (2 * (0.144 + (1.2 * (0.9 - 0.12 * x2) / 400)));
        x42 = (-((1.2 * (0.9 - 0.12 * x2) / 10) - 0.84 - 0.096 * x2) - Math.sqrt((((1.2 * (0.9 - 0.12 * x2) / 10) - 0.84 - 0.096 * x2) * ((1.2 * (0.9 - 0.12 * x2) / 10) - 0.84 - 0.096 * x2)) - 4 * ((0.144 + (1.2 * (0.9 - 0.12 * x2) / 400))) * ((1.2 * (0.9 - 0.12 * x2) + 0.82 * x2 - 0.8 - 0.02 * x2 * x2)))) / (2 * (0.144 + (1.2 * (0.9 - 0.12 * x2) / 400)));
        return new Pair(x41, x42);
    }

    public static Pair solveBlock2P3(double x2) {
        double x1, p31, p32, x31, x32;
        x1 = (1.2 * x2 + 1) / 10;
        Pair x4 = solveBlock2X4(x2);


        x31 = (1.2 * x4.x - x2 + 1 + 10 * x1) / 10;
        x32 = (1.2 * x4.y - x2 + 1 + 10 * x1) / 10;

        p31 = (x31 - x1) / ((1 - x31) * Math.exp(x4.x / (1 + (x4.x / 20))));
        p32 = (x32 - x1) / ((1 - x32) * Math.exp(x4.y / (1 + (x4.y / 20))));

        System.out.println("X2: " + x2 +" X1: "+ x1 + " X41: " + x4.x + " X42: " + x4.y + " P31: " + p31 + " P32: " + p32 +" x31" +x31);

        return new Pair(p31, p32);
    }

    public static void p() {
        double x1, p3, x2;
        x1 = 0.0965801;
        p3 = 0.07;
        x2 = -0.0284992;
        System.out.println(x1 + "-x+" + p3 + "(1-x)*exp(y/(1+(y/20)))=0;" + x2 + "-y+10*" + p3 + "*(1-x)*exp(y/(1+(y/20)))-0.2(y+5)=0");
    }

    public static void main(String[] arg) throws IOException {
         /*

         -x+0.11*(1-x)*exp(y/(1+(y/20)))=0; -y+10*0.11*exp(y/(1+(y/20)))(1-x)-0.2*(y+5)=0
         (x1)-x3+p3(1-x3)*exp(x4/(1+(x4/20)))=0; (x2)-x4+10*p3*(1-x3)*exp(x4/(1+(x4/20)))-0.2(x4+5)
          */

        double p2, x2;
        double start = -1;
        double end = 1.9;
        double h = 0.05;
        int n = ((int) ((end - start) / h)) + 1;
        double[] p21Data = new double[n];
        double[] p31Data = new double[n];
        double[] p22Data = new double[n];
        double[] p32Data = new double[n];
        int i = 0;
        for (x2 = start; x2 < end; x2 += h) {
            p2 = (1.2 * x2 + 1) / ((-1.2 * x2 + 9) * Math.exp(x2 / (1 + (x2 / 20))));
            Pair p3 = solveBlock2P3(x2);
            p21Data[i] = p2;
            p22Data[i] = p2;
            p31Data[i] = p3.x;
            p32Data[i] = p3.y;
            i++;
        }


        // Create Chart
        Chart chart = new Chart(1000, 800);
        chart.setChartTitle("Sample Chart");
        chart.setXAxisTitle("p2");
        chart.setYAxisTitle("p3");
        chart.addSeries("21", p21Data, p31Data);
        chart.addSeries("22", p22Data, p32Data);
        chart.addSeries("11", new double[]{0.117367, 0.117367}, new double[]{0, 0.3});
        chart.addSeries("12", new double[]{0.0422328, 0.0422328}, new double[]{0, 0.3});

        chart.addSeries("p2=0", new double[]{0, 0.15}, new double[]{0, 0});
        chart.addSeries("p3=0", new double[]{0, 0}, new double[]{0.3, 0});
        // Show it
        new SwingWrapper(chart).displayChart();

        BitmapEncoder.savePNG(chart, "./Sample_Chart.png");
        p();

    }
}



