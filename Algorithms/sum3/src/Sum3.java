import java.io.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ingvard
 * Date: 29.03.13
 * Time: 2:29
 * To change this template use File | Settings | File Templates.
 */
public class Sum3 {
    static int is(int[] arr,int[] sub) throws IOException {

        int temp,count=0;
        for(int i=1; i < arr.length; i++)
        {
            for(int j=0; j<i;j++)
            {
                if(arr[sub[j]] > arr[sub[i]])
                {
                    temp=sub[i];
                    sub[i]=sub[j];
                    sub[j]=temp;
                    count++;
                }

            }
        }
        return count;
    }
    public static int[] sum3(int[] arr,int[] sub)
    {
        int n=arr.length;
        int b,a,c,k,l;
        int[] res = new int[3];
        for(int i=0; i<n-3;i++) {
            a = arr[sub[i]];
            k = i+1;
            l = n-1;
            while (k<l){
                b = arr[sub[k]];
                c = arr[sub[l]];
                if (a+b+c == 0) {
                    //System.out.println(arr[sub[i]]+" "+arr[sub[k]]+" "+arr[sub[l]]);
                    res[0]=sub[i];
                    res[1]=sub[k];
                    res[2]=sub[l];
                    return res;
                }
                else if (a+b+c > 0){
                    l = l - 1;
                }
                else
                {
                     k = k + 1;
                }
            }
         }
        res[0]=-1;
        return res;
    }
    public static void main(String[] arg) throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\ingvard\\IdeaProjects\\Sum3\\src\\input.txt"));
        int k,n;
        String line;
        line = reader.readLine();
        String [] token = line.split(" ");
        n=Integer.valueOf(token[0]);
        k=Integer.valueOf(token[1]);
        int[][] arr = new int[n][k];
        int[][] subarr = new int[n][k];
        int i=0;
        while((line = reader.readLine()) != null)
        {
            token=line.split(" ");
            int j=0;
            for(String str : token)
            {
                arr[i][j]=Integer.valueOf(str);
                subarr[i][j]=j;
                j++;
            }
            i++;
        }

        BufferedWriter output = new BufferedWriter(new FileWriter("C:\\Users\\ingvard\\IdeaProjects\\Sum3\\src\\output.txt"));
        int[] res;
        for(i=0; i < n;i++)
        {
            is(arr[i],subarr[i]);
            res=sum3(arr[i],subarr[i]);
            if(res[0] != -1)
            {
                Arrays.sort(res);
                output.write((res[0]+1)+" "+(res[1]+1)+" "+(res[2]+1)+" \n");
            }
            else output.write(-1+"\n");
        }
        output.close();

    }
}
