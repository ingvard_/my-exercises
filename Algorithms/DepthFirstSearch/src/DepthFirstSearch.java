import java.io.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ingvard
 * Date: 21.03.13
 * Time: 18:58
 * To change this template use File | Settings | File Templates.
 */
public class DepthFirstSearch {
   public static int[] u;
   public static void connectedComponents(List<Integer>[] g,int x)
   {
       u[x]=1;
       for(int i=0; i<g[x].size();i++)
       {
           if(u[g[x].get(i)] == 0)
               connectedComponents(g,g[x].get(i));
       }

   }
   public static void main(String[] arg) throws IOException
   {
       BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\ingvard\\IdeaProjects\\DepthFirstSearch\\src\\input.txt"));
       String line=reader.readLine();
       String[] token = line.split(" ");
       int n=Integer.valueOf(token[0]);
       @SuppressWarnings("unchecked")
       List<Integer>[] lists = new List[n+1];
       for (int i = 0; i <= n; i++)
           lists[i] = new ArrayList<Integer>();
       u = new int[n+1];
       for(int i=0;i <u.length;i++)
           u[i]=0;
       while ((line=reader.readLine()) != null)
       {
            token=line.split(" ");
            lists[Integer.valueOf(token[0])].add(Integer.valueOf(token[1]));
            lists[Integer.valueOf(token[1])].add(Integer.valueOf(token[0]));
       }
       int count=0;
       for(int i=1;i<=n;i++)
       {
          // System.out.print(i+": ");
          // for(int j=0;j<lists[i].size();j++)
          // {

               //System.out.print(lists[i].get(j)+" ");
           //}
           //System.out.println(" ");
           if(u[i] == 0)
           {
               count++;
               connectedComponents(lists,i);
           }
       }
       System.out.print(count);

   }
}
