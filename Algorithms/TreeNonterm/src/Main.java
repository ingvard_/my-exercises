import java.util.HashSet;

public class Main {

    public static void addItemInTree(Node root,int key){
        if(root.key >= key){
            if(root.left != null){
                addItemInTree(root.left, key);
            }   else {
                root.left = new Node(key);
            }
        }   else {
            if(root.right != null){
                addItemInTree(root.right, key);
            }   else {
                root.right = new Node(key);
            }
        }

    }

    public static void sortTree(Node root){
        if(root.left != null){
            sortTree(root.left);
        }
        System.out.println(root.key);
        if(root.right != null){
            sortTree(root.right);
        }

    }
    public static Result depthTree(Node root,Result result){
        Result leftResult = result, righResult = result;
        if(root.left != null){
            leftResult = depthTree(root.left, new Result(root.key,result.depth+1));
        }
        if(root.right != null){
            righResult= depthTree(root.right, new Result(root.key,result.depth+1));
        }
        if(righResult.depth > leftResult.depth){
            return righResult;
        }
        else {
            return leftResult;
        }
    }
    public static void main(String[] args) {

        Node tree = new Node(34);
        addItemInTree(tree,12);
        addItemInTree(tree,144);
        addItemInTree(tree,3);
        addItemInTree(tree,132);
        addItemInTree(tree,54);
        addItemInTree(tree,15);
        addItemInTree(tree,2);
        addItemInTree(tree,34);
       // sortTree(tree);

        Result result = depthTree(tree, new Result(tree.key, 0));
        System.out.println(result.depth + " " +result.value);


        /// Matrix task 2
        int n = 4;
        int m = 4;
        char a[][] = {{'a','b','c','d'},{'1','2','f','4'},{'3','x','0','y'},{'z','m','q','i'}};
        for(int k=n-1; k >= 0; k--){
            int i=k,j=0;
            while(true){
                if(i < m && j < n){
                    System.out.print(a[j][i] + " ");
                    i++;
                    j++;
                } else {
                    break;
                }
            }
            System.out.println(" ");
        }

        for(int k=1; k < n; k++){
            int i=0,j=k;
            while(true){
                if(i < m && j < n){
                    System.out.print(a[j][i] + " ");
                    i++;
                    j++;
                } else {
                    break;
                }
            }
            System.out.println(" ");
        }
        // Task 3

        //Task 4
        int arr[] = {1,2,3,1,2,4,5,5};
        int brr[] = {1,4,2,5,5};

        //Task 5
        int mult[] = {-13, 12 , -1 ,1,100, -42, -1, -4, 100};
        int ans = 0;
        int prod = 1;
        int minNeg = 1;
        int minPos = 1;
        int left = 0;
        int left_neg = 0;
        int left_pos = 0;
        int right = 0;

        for(int i = 0; i < mult.length; i++){
            prod *= mult[i];
            if(prod > 0) {
                if(ans < (prod / minPos)){
                    ans = (prod / minPos);
                    right = i;
                    left = left_pos;
                }
            } else {
                if(ans < (prod / minNeg)){
                    ans = prod / minNeg;
                    right = i;
                    left = left_neg;
                }
            }
            if((mult[i] > minNeg || minNeg == 1) && mult[i] < 0){
                minNeg = mult[i];
                left_neg = i;
            }
            if(mult[i] < minPos && mult[i] > 0){
                minPos = mult[i];
                left_pos = i;
            }
        }

        System.out.println("Max prod: "+ans + " "+ left+ " : " +right);






    }
}
