/**
 * Created by maksim on 21.03.15.
 */
public class Result {
    int value;
    int depth;
    Result(int value, int depth){
        this.value = value;
        this.depth = depth;
    }
}
