/**
 * Created with IntelliJ IDEA.
 * User: ingvard
 * Date: 18.03.13
 * Time: 16:13
 * To change this template use File | Settings | File Templates.
 */
import java.io.*;
public class Sum2 {
    public static void main(String[] arg) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("/Users/ingvard/IdeaProjects/Sum2/src/input.txt"));
        int k,n;
        String line;
        line = reader.readLine();
        String [] token = line.split(" ");
        n=Integer.valueOf(token[0]);
        k=Integer.valueOf(token[1]);
        int[][] arr = new int[n][k];
        int i=0;
        while((line = reader.readLine()) != null)
        {
             token=line.split(" ");
            int j=0;
            for(String str : token)
            {
                arr[i][j]=Integer.valueOf(str);
                j++;
            }
            i++;
        }
        for(i=0; i < n;i++)
        {
            int flag=0;
            for(int j=0; j < k;j++)
            {
                for(int z=j+1; z < k && flag == 0;z++)
                {
                    //System.out.println("Debug: "+Math.abs(arr[i][j])+" "+Math.abs(arr[i][z]));
                    if(Math.abs(arr[i][j]) == Math.abs(arr[i][z]))
                    {
                        System.out.println(j+" "+z);
                        flag=1;
                        break;
                    }

                }
            }
            if(flag == 0) System.out.println(-1);
        }


    }
}
