import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: ingvard
 * Date: 29.04.13
 * Time: 23:30
 * To change this template use File | Settings | File Templates.
 */
public class KnapsackProblem {
    public static void main(String [] arg) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\ingvard\\IdeaProjects\\KnapsackProblem\\src\\input.txt"));
        String line = reader.readLine();
        String [] token = line.split(" ");
        int n = Integer.valueOf(token[1]);
        int w = Integer.valueOf(token[0]);
        int pair [][] = new int[n][2];
        for(int i=0; (line = reader.readLine()) != null ;i++)
        {
            token = line.split(" ");
            pair[i][0]=Integer.valueOf(token[0]);
            pair[i][1]=Integer.valueOf(token[1]);
        }
        int arr [][] = new int[n+1][w+1];
        for (int i=0;i < n; i++)
            arr[i][0]=0;
        for (int i=0;i < w; i++)
            arr[0][i]=0;

        for(int i = 1; i <= w; i++)
        {
            for(int k=1; k <= n;k++)
            {
                if(i >= pair[k-1][0])
                {
                    arr[k][i] = Math.max(arr[k-1][i],arr[k-1][i-pair[k-1][0]]+pair[k-1][1]);
                }
                else
                {
                    arr[k][i] = arr[k-1][i];
                }
            }
        }


        for(int i = 1; i <= n; i++)
        {
            for(int k=1; k <= w;k++)
            {
                System.out.print(arr[i][k]+" ");
            }
            System.out.println(" ");
        }


    }
}
