/**
 * Created with IntelliJ IDEA.
 * User: ingvard
 * Date: 18.03.13
 * Time: 16:54
 * To change this template use File | Settings | File Templates.
 */
import java.io.*;
public class BuildingHeap {
    public static void shiftUp(int[] a,int i)
    {
        int temp;
        if(i != 0)
        {
            if(a[i] > a[i/2])
            {
                temp=a[i/2];
                a[i/2]=a[i];
                a[i]=temp;
                shiftUp(a,i/2);
            }
        }

    }
    public static void shiftDown(int[] a,int i,int count)
    {
        if(2*i < count)
            if(a[i] < a[2*i] && a[i] < a[2*i+1]){
                int temp;
                if(a[2*i] > a[2*i+1])
                 {
                    temp=a[i];
                    a[i]=a[2*i];
                    a[2*i]=temp;
                    shiftDown(a,i*2,count);
                 }
                else
                {
                    temp=a[i];
                    a[i]=a[2*i+1];
                    a[2*i+1]=temp;
                    shiftDown(a,2*i+1,count);
                }
            }

    }
    public static int extractMax(int[] a,int count)
    {
        int result=a[1];
        if(count > 0 && count <= a.length)
        {
            a[1]=a[count-1];
            shiftDown(a,0,count-2);
            count--;
        }
        return result;
    }
    public static void main(String[] arg) throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader("/Users/maksim/IdeaProjects/BuildingHeap/src/input.txt"));
        String line = reader.readLine();
        int n=Integer.valueOf(line);
        int[] arr = new int[n];
        int countInHeap=0;
        line = reader.readLine();
        String[] token = line.split(" ");
        int i=0;
        for(String str : token)
        {
            arr[i]=Integer.valueOf(str);
            shiftUp(arr,i);
            countInHeap++;
            i++;
        }
        //arr[0]=1;
        //shiftDown(arr,0,countInHeap);
        //7 5 2 3 1
        BufferedWriter output = new BufferedWriter(new FileWriter("/Users/maksim/IdeaProjects/BuildingHeap/src/output.txt"));
        for(i=0;i<n;i++)
            output.write(arr[i]+" ");
        output.close();
    }
}
