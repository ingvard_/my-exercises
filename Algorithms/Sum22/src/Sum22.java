/**
 * Created with IntelliJ IDEA.
 * User: ingvard
 * Date: 18.03.13
 * Time: 16:13
 * To change this template use File | Settings | File Templates.
 */
import java.io.*;
import java.util.*;

public class Sum22 {

        static int is(int[] arr,int[] sub) throws IOException{

            int temp,count=0;
            for(int i=1; i < arr.length; i++)
            {
                for(int j=0; j<i;j++)
                {
                    if(arr[sub[j]] > arr[sub[i]])
                    {
                        temp=sub[i];
                        sub[i]=sub[j];
                        sub[j]=temp;
                        count++;
                    }

                }
            }
            return count;
        }

    static int binarySearch(int[] search,int[] sub,int s,int e, int find) {
        int start, end, midPt;
        start = s;
        end = e - 1;
        while (start <= end) {
            midPt = (start + end) / 2;
            if (search[sub[midPt]] == find) {
                return midPt;
            } else if (search[sub[midPt]] < find) {
                start = midPt + 1;
            } else {
                end = midPt - 1;
            }
        }
        return -1;
    }

    public static int way3(int[] arr,int[] sub)
    {
        int r,f;
        for(int i=0; i < arr.length;i++)
        {
            for(int j=0; j < arr.length-1;j++)
            {
                if(sub[i] < sub[j])
                {
                    f=((-1)*(arr[sub[i]]+arr[sub[j]]));
                    r=binarySearch(arr,sub,j+1,arr.length, f);

                    if(r != -1 && sub[r] > sub[j])
                    {
                        System.out.println((sub[i]+1)+" "+(sub[j]+1)+" "+(sub[r]+1));
                        return 1;
                    }
                }

            }
        }
        System.out.println(-1);
        return -1;
    }
    public static void main(String[] arg) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\ingvard\\IdeaProjects\\Sum22\\src\\input.txt"));
        int k,n;
        String line;
        line = reader.readLine();
        String [] token = line.split(" ");
        n=Integer.valueOf(token[0]);
        k=Integer.valueOf(token[1]);
        int[][] arr = new int[n][k];
        int[][] subarr = new int[n][k];
        int i=0;
        while((line = reader.readLine()) != null)
        {
            token=line.split(" ");
            int j=0;
            for(String str : token)
            {
                arr[i][j]=Integer.valueOf(str);
                subarr[i][j]=j;
                j++;
            }
            i++;
        }

        BufferedWriter output = new BufferedWriter(new FileWriter("C:\\Users\\ingvard\\IdeaProjects\\Sum22\\src\\output.txt"));
        for(i=0; i < n;i++)
        {
            is(arr[i],subarr[i]);
            way3(arr[i],subarr[i]);
        }
       // for(i=0; i < k;i++)
       //     System.out.print(arr[0][i]+" ");
       // System.out.println(" ");
      //  for(i=0; i < k;i++)
       //     System.out.print(subarr[0][i]+" ");
       // System.out.println(" ");
       // for(i=0; i < k;i++)
       //     System.out.print(arr[0][subarr[0][i]]+" ");
       // System.out.println(" ");
        //2 -3 4 10 5
        output.close();


    }
}
